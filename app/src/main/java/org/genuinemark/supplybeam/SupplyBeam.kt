package org.genuinemark.supplybeam

import android.app.Activity
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDexApplication
import androidx.preference.PreferenceManager
import org.genuinemark.supplybeam.utils.Constants
import org.genuinemark.supplybeam.utils.SharedPreferenceUtils
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.genuinemark.supplybeam.api.response.ApiService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class SupplyBeam : MultiDexApplication() {


    companion object {
        var currentActivity: Activity? = null
        var dialogShownForUpdate: Int = 0
    }

    override fun onCreate() {
        super.onCreate()
        //FirebaseApp.initializeApp(this)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        PreferenceManager.getDefaultSharedPreferences(this).edit()
            .putBoolean(Constants.PREFERENCE_FIRST_TIME, false).apply()

        provideApiService()
        //AppDatabase.initDB(this)

//        getFCMToken()

    }

    private fun getFCMToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("SupplyBeam", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token
                SharedPreferenceUtils.saveFCMToken(this, token)
                //Log.e("App FCM Token:- ", token)
            })
    }

    fun provideApiService(): ApiService{
        val builder =  OkHttpClient.Builder()

        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        builder.connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.MINUTES)
            .writeTimeout(10, TimeUnit.MINUTES)
            .retryOnConnectionFailure(true)
            .addInterceptor(httpLoggingInterceptor)

        val okHttpClient: OkHttpClient = builder.build()
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ApiService::class.java)
    }

}