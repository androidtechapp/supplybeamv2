package org.genuinemark.supplybeam.ui.activities

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.vision.barcode.Barcode
import info.androidhive.barcode.BarcodeReader
import kotlinx.android.synthetic.main.check_in_out_act.*
import org.genuinemark.supplybeam.BuildConfig
import org.genuinemark.supplybeam.R
import org.genuinemark.supplybeam.adapter.CheckInOutAdapter
import org.genuinemark.supplybeam.api.input.CheckInOutInput
import org.genuinemark.supplybeam.api.response.JsonObjectResponse
import org.genuinemark.supplybeam.api.response.Resource
import org.genuinemark.supplybeam.api.response.Status
import org.genuinemark.supplybeam.base.BaseActivity
import org.genuinemark.supplybeam.databinding.CheckInOutActBinding
import org.genuinemark.supplybeam.databinding.CheckInScanActivityBinding
import org.genuinemark.supplybeam.models.CheckInOutModel
import org.genuinemark.supplybeam.utils.CommonUiItems
import org.genuinemark.supplybeam.utils.SystemUtility
import org.genuinemark.supplybeam.viewModel.HomeViewModel

class CheckInBarCodeScanActivity : BaseActivity(), BarcodeReader.BarcodeReaderListener {


    private lateinit var responseList: ArrayList<CheckInOutModel.CheckInOutData>

    private lateinit var alreadyScanDialog: AlertDialog
    private lateinit var barcodeReader: BarcodeReader
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var binding: CheckInOutActBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.check_in_out_act)

        initial()

    }

    private fun initial() {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

//        binding.headerLayout.ivBackBtn.setOnClickListener(this)
//        binding.headerLayout.visibility = View.VISIBLE
//        binding.headerLayout.ivTxtHeading.setText(getString(R.string.check_in))

        // get the barcode reader instance

        barcodeReader =
            (supportFragmentManager.findFragmentById(R.id.barcode_scanner) as BarcodeReader?)!!
        alreadyScanDialog = CommonUiItems.myDialog(
            this,
            getString(R.string.warning),
            getString(R.string.alreeady_scann),
            getString(R.string.ok),
            null
        )

        setAdapter()
        callApi()
        observeViewModel(homeViewModel)
    }


    private val adapter by lazy {
        CheckInOutAdapter(mutableListOf(), this)
    }
    private fun setAdapter() {
        try {

            binding.listRecyclerviewCheckin.layoutManager =
                LinearLayoutManager(this) as RecyclerView.LayoutManager
            binding.listRecyclerviewCheckin.adapter = adapter

            val dividerItemDecoration = DividerItemDecoration(
                binding.listRecyclerviewCheckin.context,
                LinearLayoutManager.VERTICAL
            )
            binding.listRecyclerviewCheckin.addItemDecoration(dividerItemDecoration)


        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun callApi() {


        val checkInInput = CheckInOutInput(
            intent?.extras?.getString("id")
        )
        if (SystemUtility.isNetworkAvailable(this)) {
            homeViewModel.setCheckInOutInput(checkInInput)

        } else {
            onNoInternet()
        }
    }


    private fun observeViewModel(homeViewModel: HomeViewModel) {
        try {

            /**
             * Call Observable for chekc-in
             */
            homeViewModel.getCheckInOUtObservable()?.observe(this,
                Observer<Resource<JsonObjectResponse<CheckInOutModel>>> { response ->
                    if (response != null) {
                        if (response.headers != null) {
                            processHeaders(response.headers)
                        }
                        when {
                            response.status == Status.LOADING -> {
                                enableLoadingBar(true)
                            }

                            response.status == Status.SUCCESS -> {
                                enableLoadingBar(false)
                                responseList = response.data?.`object`?.itemList!!
                            }

                            response.status == Status.ERROR -> {
                                enableLoadingBar(false)
                            }

                            response.status == Status.FAILURE -> {
                                enableLoadingBar(false)
                            }

                            response.status == Status.FORCE_UPDATE -> {

                            }
                        }
                    }
                })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onScanned(barcode: Barcode) {
        // single barcode scanned
        barcodeReader?.playBeep();
        Log.v("onScanned", barcode.displayValue)
        Log.v("onScanned", barcode.rawValue)

        for (item in responseList) {
            if (item.qrcode == barcode.rawValue) {

                adapter.addData(item)

            } else {
                runOnUiThread(Runnable {
                    if (alreadyScanDialog != null && !alreadyScanDialog!!.isShowing()) {
                        alreadyScanDialog!!.show()
                    }
                })
            }
        }


    }

    override fun onScannedMultiple(list: List<Barcode>) {
        // multiple barcodes scanned
        barcodeReader?.playBeep();
        for (barcode in list) {

            Log.v("onScannedMultiple", barcode.displayValue)
        }
    }

    override fun onBitmapScanned(sparseArray: SparseArray<Barcode>) {
        // barcode scanned from bitmap image
    }

    override fun onScanError(s: String) {
        // scan error
        onInfo(s)
    }


}