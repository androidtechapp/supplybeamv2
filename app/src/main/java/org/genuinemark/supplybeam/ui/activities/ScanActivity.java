package org.genuinemark.supplybeam.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;

import org.genuinemark.supplybeam.R;
import org.genuinemark.supplybeam.base.BaseActivity;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import info.androidhive.barcode.BarcodeReader;

public class ScanActivity extends BaseActivity implements BarcodeReader.BarcodeReaderListener {

    BarcodeReader barcodeReader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // get the barcode reader instance
        //barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_scanner);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isCameraPermission()){
            barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_scanner);
        }
    }


    /*override fun onResume() {
        super.onResume()
        if (isCameraPermission()){
            barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_scanner);
        }

    }*/

    @Override
    public void onScanned(Barcode barcode) {

        // playing barcode reader beep sound
        barcodeReader.playBeep();

        // ticket details activity by passing barcode
        //Intent intent = new Intent(ScanActivity.this, TicketResultActivity.class);
        //intent.putExtra("code", barcode.displayValue);
        //startActivity(intent);
    }

    @Override
    public void onScannedMultiple(List<Barcode> list) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String s) {
        Toast.makeText(getApplicationContext(), "Error occurred while scanning " + s, Toast.LENGTH_SHORT).show();
    }

    /*@Override
    public void onCameraPermissionDenied() {
        finish();
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }


    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {

            PERMISSION_REQUEST_QR_CAMERA -> if(resultCode == Activity.RESULT_OK){

            }

        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when(requestCode){



            Constants.PERMISSION_REQUEST_QR_CAMERA ->
            if(grantResults.size > 0  && grantResults[0] == PackageManager.PERMISSION_GRANTED){

            } else{

            }

            *//*if(grantResults.size > 0  && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){

                } else{

                }*//*

        }
    }*/

}
