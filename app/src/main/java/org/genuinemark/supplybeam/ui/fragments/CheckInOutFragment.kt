package org.genuinemark.supplybeam.ui.fragments


import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import info.androidhive.barcode.BarcodeReader
import kotlinx.android.synthetic.main.header_layout.view.*
import org.genuinemark.supplybeam.R
import org.genuinemark.supplybeam.base.BaseFragment
import org.genuinemark.supplybeam.databinding.FragmentCheckInOutBinding
import org.json.JSONObject
import com.google.android.gms.vision.barcode.Barcode
import android.util.SparseArray
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import org.genuinemark.supplybeam.api.input.CheckInInput
import org.genuinemark.supplybeam.api.input.CheckInOutInput
import org.genuinemark.supplybeam.api.response.JsonObjectResponse
import org.genuinemark.supplybeam.api.response.Resource
import org.genuinemark.supplybeam.api.response.Status
import org.genuinemark.supplybeam.models.CheckInModel
import org.genuinemark.supplybeam.models.CheckInOutModel
import org.genuinemark.supplybeam.utils.CommonUiItems
import org.genuinemark.supplybeam.utils.SystemUtility
import org.genuinemark.supplybeam.viewModel.HomeViewModel


/**
 * A simple [Fragment] subclass.
 */
class CheckInOutFragment : BaseFragment(),  BarcodeReader.BarcodeReaderListener{

    private lateinit var responseList: ArrayList<CheckInOutModel.CheckInOutData>
    private lateinit var homeViewModel: HomeViewModel
    lateinit var binding: FragmentCheckInOutBinding
    // var mScannerView: ZXingScannerView? = null
    var barcodeReader: BarcodeReader? = null
    var alreadyScanDialog: AlertDialog? =null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentCheckInOutBinding.inflate(inflater, container, false).also {  }

        //mScannerView = ZXingScannerView(activity)
        init()
        return binding.root
    }

    fun init(){

        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        binding.headerLayout.ivBackBtn.setOnClickListener(this)
        binding.headerLayout.visibility = View.VISIBLE
        binding.headerLayout.ivTxtHeading.setText(getString(R.string.check_in))

        // get the barcode reader instance
        barcodeReader = requireActivity().supportFragmentManager.findFragmentById(R.id.barcode_scanner) as BarcodeReader?
        alreadyScanDialog = CommonUiItems.myDialog(
            context,
            getString(R.string.warning),
            getString(R.string.alreeady_scann),
            getString(R.string.ok),
            null
        )

        callApi()

    }

    private fun callApi() {

        val checkInInput = CheckInOutInput(
            "339"
        )
        if (SystemUtility.isNetworkAvailable(activity!!)) {
            homeViewModel.setCheckInOutInput(checkInInput)
        } else {
            onNoInternet()
        }
    }



    private fun observeViewModel(homeViewModel: HomeViewModel) {
        try {

            /**
             * Call Observable for chekc-in
             */
            homeViewModel.getCheckInOUtObservable()?.observe(viewLifecycleOwner,
                Observer<Resource<JsonObjectResponse<CheckInOutModel>>> { response ->
                    if (response != null) {
                        if (response.headers != null) {
                            processHeaders(response.headers)
                        }
                        when {
                            response.status == Status.LOADING -> {
                                enableLoadingBar(true)
                            }

                            response.status == Status.SUCCESS -> {
                                enableLoadingBar(false)
                                responseList= response.data?.`object`?.itemList!!
                            }

                            response.status == Status.ERROR -> {
                                enableLoadingBar(false)
                            }

                            response.status == Status.FAILURE -> {
                                enableLoadingBar(false)
                            }

                            response.status == Status.FORCE_UPDATE -> {

                            }
                        }
                    }
                })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun onScanned(barcode: Barcode) {
        // single barcode scanned
        barcodeReader?.playBeep();
        Log.v("onScanned", barcode.displayValue)


        /*runOnUiThread(Runnable {
            if (alreadyScanDialog != null && !alreadyScanDialog!!.isShowing()) {
                alreadyScanDialog!!.show()
                //PlayAudio.playBuzz(this@CheckInScanActivity)
            }
        })*/
    }

    override fun onScannedMultiple(list: List<Barcode>) {
        // multiple barcodes scanned
        barcodeReader?.playBeep();
        for (barcode in list) {

            Log.v("onScannedMultiple", barcode.displayValue)
        }
    }

    override fun onBitmapScanned(sparseArray: SparseArray<Barcode>) {
        // barcode scanned from bitmap image
    }

    override fun onScanError(s: String) {
        // scan error
        onInfo(s)
    }





    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){

            R.id.ivBackBtn-> {
                requireActivity().onBackPressed()
            }


        }
    }

}
