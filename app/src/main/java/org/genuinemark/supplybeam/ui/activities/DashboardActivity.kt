package org.genuinemark.supplybeam.ui.activities

import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.genuinemark.supplybeam.R
import org.genuinemark.supplybeam.base.BaseActivity
import org.genuinemark.supplybeam.databinding.ActivityDashboardBinding

class DashboardActivity : BaseActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener,
    NavController.OnDestinationChangedListener{


    lateinit var binding: ActivityDashboardBinding
    lateinit var navController: NavController
    private val bottomBarHolderDestinations = listOf(R.id.homeFragment,R.id.profileFragment,R.id.inventoryFragment,R.id.historyFragment)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard)
        binding.bottomNavigation.clearAnimation()
        navController = setNavigationController()
    }

    private fun setNavigationController(): NavController {
        val navController = Navigation.findNavController(this, R.id.main_nav_fragment)
        navController.addOnDestinationChangedListener(this)
        binding.bottomNavigation.setupWithNavController(navController)
        binding.bottomNavigation.setOnNavigationItemSelectedListener(this)
        navController.addOnDestinationChangedListener { _, _, _ -> hideKeyboard() }
        return navController
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when (p0.itemId) {
            R.id.homeFragment -> {
                Handler().postDelayed({
                    navController.navigate(R.id.homeFragment)
                }, 200)
                return true
            }

            R.id.profileFragment -> {
                Handler().postDelayed({
                    navController.navigate(R.id.profileFragment)
                }, 200)
                return true
            }

            R.id.inventoryFragment -> {
                Handler().postDelayed({
                    //navController.navigate(R.id.DocumentListFragment)
                    navController.navigate(R.id.inventoryFragment)
                }, 200)
                return true
            }

            R.id.historyFragment -> {
                Handler().postDelayed({
                    navController.navigate(R.id.historyFragment)
                }, 200)
                return true
            }
        }
        return false
    }

    override fun onDestinationChanged(
        controller: NavController,
        destination: NavDestination,
        arguments: Bundle?
    ) {
        if (destination.id in bottomBarHolderDestinations) {
            showBottomNavigation()
        } else {
            hideBottomNavigation()
        }
    }




    private fun hideBottomNavigation() {
        with(binding.bottomNavigation) {
            if (visibility == android.view.View.VISIBLE && alpha == 1f) {
                animate()
                    .alpha(0f)
                    .withEndAction { visibility = android.view.View.GONE}
                    .duration = 100
            }
        }
    }

    private fun showBottomNavigation() {
        with(binding.bottomNavigation) {
            visibility = android.view.View.VISIBLE
            animate()
                .alpha(1f)
                .duration = 100
        }
    }
}