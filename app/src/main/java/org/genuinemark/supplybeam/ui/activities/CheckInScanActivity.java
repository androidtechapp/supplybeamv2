package org.genuinemark.supplybeam.ui.activities;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.vision.barcode.Barcode;

import org.genuinemark.supplybeam.R;
import org.genuinemark.supplybeam.utils.CommonUiItems;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.androidhive.barcode.BarcodeReader;
import info.androidhive.barcode.ScannerOverlay;


public class CheckInScanActivity extends AppCompatActivity implements BarcodeReader.BarcodeReaderListener {

    //List<CheckinRequest> checkinRequests =new ArrayList<>();
    BarcodeReader barcodeReader ;
    @BindView(R.id.rel)
    View viewFrag;
    ScannerOverlay scannerOverlay;
    //MyGlobalListener myGlobalListener ;
    //List<QrList> qrListList = new ArrayList<>();
    @BindView(R.id.list_recyclerview_checkin)
    RecyclerView recyclerView;

    @BindView(R.id.typeTxt)
    TextView typeTxt;
    @BindView(R.id.pCount)
    TextView prodCount;
    int pCount=0;
    Map<String, Integer> qrcodeMap;
    //List<ProductDetailsBean> productList;
    //MyAdapter adapter;
    //CheckinRequest checkinRequest;
    //List<BatchListData> batchListDataList=new ArrayList<>();

    String brandID ,brandName,productID,productName,mQRID;
    //@BindView(R.id.pBrand)
    //TextView pBrand;
    //@BindView(R.id.pBatch)
    //TextView pBatch;
   // @BindView(R.id.pProduct)
   // TextView pProduct;
  //  @BindView(R.id.pQuantity)
  //  TextView pQuantity;
    @BindView(R.id.btnStartStop)
    Button btnStartStop;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
   // @BindView(R.id.progressBarRL)
   // RelativeLayout progressBarRL;
    ImageView imgTaggingDialog;
    private String brand,batch,product;
    private int quantity=0;
    boolean isScanning = false;
    boolean isContinue = false;
  //  private List<Division> divisionList = new ArrayList<>();
    private String actionType = "";
    String imageString= "";
    static final int REQUEST_IMAGE_CAPTURE_RDSO_CERTIFICATE = 111;

    String loginType = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.check_in_scan_activity);

        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
          loginType =  "L1";  // bundle.getString("login_type");
        }
       // actionType = getIntent().getStringExtra(Constants.KEY_TYPE);
       // typeTxt.setText(actionType);
       // myGlobalListener = new MyGlobalListener();
        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_scanner);
        scannerOverlay = findViewById(R.id.scannerOverlay);
       // viewFrag.getViewTreeObserver().addOnGlobalLayoutListener(myGlobalListener);
        barcodeReader.setUserVisibleHint(false);
    //    productList = new ArrayList<ProductDetailsBean>();
        qrcodeMap = new HashMap<>();

        //recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //adapter = new MyAdapter(CheckInScanActivity.this, R.layout.checkin_list_item);
        //recyclerView.setAdapter(adapter);
        alreadyScanDialog = CommonUiItems.myDialog(CheckInScanActivity.this, "Warning!!", "Already scanned", "ok", null);
    }

    AlertDialog alreadyScanDialog;
    @Override
    public void onScanned(Barcode barcode) {
        String qr = barcode.rawValue;

        if(!qrcodeMap.containsKey(qr)) {
            qrcodeMap.put(qr,1);

            barcodeReader.playBeep();
            //requestProductDetails(barcode.rawValue);
        }else{
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(alreadyScanDialog!=null && !alreadyScanDialog.isShowing()) {
                        alreadyScanDialog.show();
                       //Toast.makeText(this, qr, Toast.LENGTH_SHORT).show();
                     //   PlayAudio.playBuzz(CheckInScanActivity.this);
                    }
                }
            });

        }
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {
        for(Barcode barcode : barcodes){
            String qr = barcode.rawValue;

            if(!qrcodeMap.containsKey(qr)) {
                qrcodeMap.put(qr,1);
                barcodeReader.playBeep();

                //requestProductDetails(barcode.rawValue);
            }else{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    if(alreadyScanDialog!=null && !alreadyScanDialog.isShowing()) {
                        alreadyScanDialog.show();
                        //Toast.makeText(this, qr, Toast.LENGTH_SHORT).show();
                       // PlayAudio.playBuzz(CheckInScanActivity.this);
                    }
                    }
                });
            }
        }
    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {
        Toast.makeText(this, errorMessage.toString(), Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btnStartStop)
    public void onStartStopBtnClick(){
        if(isScanning) {
            barcodeReader.resumeScanning();
            scannerOverlay.setVisibility(View.VISIBLE);
            isScanning=false;
            btnStartStop.setText("Stop");
        }else{
            barcodeReader.pauseScanning();
            scannerOverlay.setVisibility(View.GONE);
            btnStartStop.setText("Start");
            isScanning=true;
        }
//        requestProductDetails("JTk0JTAwJTk5JUNBJUQ1JUY3JUVBJTBEJUQ3JUUyJTlGJTIzJTg3JUYzLiUyNSVERCVDMWVWJUE4JTBGJUYyJUQ4JURDJTkwJUIyJUY3YSVGMCVBOCUxMw==");
    }

    @OnClick(R.id.btnSubmit)
    public void onSubmitBtnClick(){
        isScanning = true;
        barcodeReader.pauseScanning();
        scannerOverlay.setVisibility(View.GONE);
        //showListDialog();
     //   if(actionType.equals(Constants.VALUE_TAGGING)){
            taggingDialog1();
     //   }else if(actionType.equals(Constants.VALUE_CHECKOUT)){
     //       taggingDialog1();
      //  }else {
       //     showListDialog();
        //}
    }

    /*private void queueDataForServer( ){
        for( ProductDetailsBean p: productList) {
            checkinRequest = new CheckinRequest();
            checkinRequest.setAction("check_in_qrcode");
            CheckinRequestData checkinRequestData = new CheckinRequestData();
            checkinRequestData.setUser_id("6");
            checkinRequestData.setVendor_id("30");
            checkinRequestData.setUser_type("");
            if (loginType.equals("L1")) {
                checkinRequestData.setUser_type("L1");
                Log.d("User tyPe", loginType);
            } else if (loginType.equals("L2")) {
                checkinRequestData.setUser_type("L2");
                Log.d("User tyPe", loginType);
            }
            BatchListData batchListData = new BatchListData();
            batchListData.setBatchId("67");
            batchListData.setBatchName("RCM0219");
            batchListData.setBrandId(p.getData().getBrandId());
            batchListData.setBrandName(p.getData().getBrandName());
            batchListData.setProductId(p.getData().getProductId());
            batchListData.setProductName(p.getData().getProductName());
            QrList qrList = new QrList();
            qrList.setQrId(p.getData().getQrId());

            qrListList.add(qrList);
            batchListData.setQrList(qrListList);

            batchListDataList.add(batchListData);
            checkinRequestData.setBatchList(batchListDataList);

            checkinRequest.setData(checkinRequestData);
            //   Log.d("List size", ""+batchListDataList.size());
            checkinRequests.add(checkinRequest);
        }
     }*/
    /*private void addProduct(ProductDetailsBean productDetailsBean){
        productList.add(productDetailsBean);
        //adapter.notifyItemInserted(position);
        prodCount.setText(String.valueOf(productList.size()));
        adapter.notifyDataSetChanged();
        adapter.notifyItemChanged(productList.size());
        recyclerView.smoothScrollToPosition(productList.size());
    }*/

    /*private void removeProduct(int position){
        productList.remove(position);
        prodCount.setText(String.valueOf(productList.size()));
        adapter.notifyDataSetChanged();
        adapter.notifyItemChanged(position);
        recyclerView.smoothScrollToPosition(position);
    }*/

    /*public void requestProductDetails(String qrcode){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBarRL.setVisibility(View.VISIBLE);
            }
        });
        ProductDetailsRequestBean requestBean = new ProductDetailsRequestBean();
        requestBean.setAction("product_details");

        Data data = new Data();
        data.setQrId(qrcode);
        requestBean.setData(data);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ProductDetailsBean> productDetailsRequestBeanCall = apiInterface.productDetailsRequest(requestBean);
        productDetailsRequestBeanCall.enqueue(new Callback<ProductDetailsBean>() {
            @Override
            public void onResponse(Call<ProductDetailsBean> call, Response<ProductDetailsBean> response) {
                Gson gson = new Gson();
                String Json = gson.toJson(response.body());
                Log.d("Response",Json.toString() );
                if(response.body().getStatus().equals("1")) {
                 *//* brandID =   response.body().getData().getBrandId();
                    brandName = response.body().getData().getBrandName();
                    productID  = response.body().getData().getProductId();
                    productName = response.body().getData().getProductName();
                    mQRID = response.body().getData().getQrId();*//*
                    queueDataForServer();


                    addProduct(response.body());
                    Log.d("ABCD", "Response : " + response.body().getStatus());
                }else{
                    showAlertDialog("Product not recognized","This product is not recognized please try with another QR-Code");
                    Log.d("ABCD", "Response : " + response.body().getStatus());
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBarRL.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void onFailure(Call<ProductDetailsBean> call, Throwable t) {
                showAlertDialog("No Internet","Please check your internet connection");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBarRL.setVisibility(View.GONE);
                    }
                });
            }
        });
    } */

    /*class MyGlobalListener implements ViewTreeObserver.OnGlobalLayoutListener{

        @Override
        public void onGlobalLayout() {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            float x,y;
            y = viewFrag.getHeight();
            x = width*(y/height);
            viewFrag.setLayoutParams(new LinearLayout.LayoutParams((int)x,(int)y));
            //Toast.makeText(CheckInScanActivity.this, "w = "+w+" h = "+h, Toast.LENGTH_SHORT).show();
            viewFrag.getViewTreeObserver().removeOnGlobalLayoutListener(myGlobalListener);
        }

    }*/

    /*class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{

        Activity context;
        int layout;
        public MyAdapter(Activity context, int layout) {
            this.context = context;
            this.layout = layout;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v = getLayoutInflater().inflate(layout,viewGroup,false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
            //ProductDetailsBean product = productList.get(i);
            //org.genuinemark.supplybeam.model.ProductDetails.ProductDetailsResponsePojo.Data data = product.getData();
            //String pname = data.getProductName();//+"( id = "+data.getProductId()+")";
            //viewHolder.pName.setText(pname);
            //viewHolder.pDesc.setText(data.getDescription());
            try {
                //viewHolder.pBrand.setText("Brand : " + data.getBrandName());
            }catch (Exception e){

            }
            try {
                viewHolder.pCounter.setText((i+1)+". ");
            }catch (Exception e){

            }
            try {
              //  viewHolder.pManufactingDate.setText(data.getManufacturingDate());
            }catch (Exception e){

            }
           // Glide.with(context).load(data.getImages().get(0)).into(viewHolder.img);
        }

        @Override
        public int getItemCount() {
            return productList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            AppCompatImageView img;
            TextView pName,pDesc,pBrand,pCounter,pManufactingDate;
            CheckBox check;
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                //img = itemView.findViewById(R.id.pImage);
                pName = itemView.findViewById(R.id.pName);
                pBrand = itemView.findViewById(R.id.pBrand);
                pDesc = itemView.findViewById(R.id.pDesc);
                check = itemView.findViewById(R.id.pCheck);
                pManufactingDate = itemView.findViewById(R.id.pManufactingDate);
                pCounter = itemView.findViewById(R.id.counter);
                check.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(!check.isChecked()){
                            showAlertForItemUnCheck(getAdapterPosition());
                        }
                    }
                });
            }

            public void showAlertForItemUnCheck(final int position){
                isScanning=false;
                onStartStopBtnClick();
                final AlertDialog.Builder builder = new AlertDialog.Builder(CheckInScanActivity.this);
                builder.setTitle("Remove Item From List")
                        .setCancelable(false)
                        .setMessage("Do you want to remove this item ?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                removeProduct(position);
                                check.setChecked(true);
                                isScanning=true;
                                onStartStopBtnClick();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                check.setChecked(true);
                                isScanning=true;
                                onStartStopBtnClick();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    }*/

    /*public void callDialog2(String brand, String product, String desc, AppCompatImageView imgUrl ){
        Dialog dialog = new Dialog(CheckInScanActivity.this);
        dialog.setContentView(R.layout.product_details_dialog);
        dialog.show();
        AppCompatImageView img;
        TextView pName,pDesc,pBrand;
        //img = dialog.findViewById(R.id.dImage);
        pName = dialog.findViewById(R.id.dProduct);
        pBrand = dialog.findViewById(R.id.dBrand);
        pDesc = dialog.findViewById(R.id.dDesc);
        pName.setText(product);
        pDesc.setText(desc);
        pBrand.setText(brand);
        //img.setImageDrawable(imgUrl.getDrawable());
        //Glide.with(CheckInScanActivity.this).load(imgUrl).into(img);

    }*/

    public void showAlertDialog(String title, String msg){
        isScanning=false;
        onStartStopBtnClick();
        final AlertDialog.Builder builder = new AlertDialog.Builder(CheckInScanActivity.this);
        builder.setTitle(title)
                .setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        isContinue = true;
                        isScanning=true;
                        onStartStopBtnClick();
                    }
                }).setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        isContinue=false;
                        //onStartBtnClick();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
        Log.d("aaaa","dialog...");
    }

    /*public void showListDialog(){
        if(productList.size()==0){
            CommonUiItems.myDialog(CheckInScanActivity.this, "Warning!!", "Your Product list is Empty.", "ok", null, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    onStartStopBtnClick();
                }
            },null);
            return;
        }
        final Dialog dialog = CommonUiItems.getCustomDialog(CheckInScanActivity.this, R.layout.dialog_list_of_products);
        RecyclerView recyclerView1 = dialog.findViewById(R.id.recyclerView);
        Button proceedBtn = dialog.findViewById(R.id.proceedBtn);
        TextView name,address,email,location,dealerCode,gst,memCategory,dpLevel;
        name = dialog.findViewById(R.id.tName);
        address = dialog.findViewById(R.id.tAddress);
        email = dialog.findViewById(R.id.tEmail);
        location = dialog.findViewById(R.id.tLocation);
        dealerCode = dialog.findViewById(R.id.tDC);
        gst = dialog.findViewById(R.id.tGst);
        memCategory = dialog.findViewById(R.id.tMem);
        dpLevel = dialog.findViewById(R.id.tDPL);
        UserRegDetails userRegDetails = UserRegDetails.getUser();
        if(userRegDetails!=null){
            name.setText(userRegDetails.getName());
            address.setText(userRegDetails.getAddress());
            email.setText(userRegDetails.getEmail());
            location.setText(userRegDetails.getLocation());
            dealerCode.setText(userRegDetails.getDealerCode());
            gst.setText(userRegDetails.getGst());
            memCategory.setText(userRegDetails.getMemCategory());
            dpLevel.setText(userRegDetails.getDpLevel());
        }

        recyclerView1.setLayoutManager(new LinearLayoutManager(this));
        MyAdapter adapter = new MyAdapter(CheckInScanActivity.this, R.layout.item_product);
        recyclerView1.setAdapter(adapter);
        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                uploadDataToServer();
                //CommonUiItems.myDialog(CheckInScanActivity.this,"Congratulations!!","Your data uploaded successfully.","ok",null).show();
            }
        });
    }*/


    private void taggingDialog1(){
        final Dialog dialog = CommonUiItems.getCustomDialog(CheckInScanActivity.this, R.layout.dialog_tagging);
        dialog.show();
        final TextView head = dialog.findViewById(R.id.head);
        final TextView productCOuntTxt = dialog.findViewById(R.id.productCountTxt);
        final TextView doc = dialog.findViewById(R.id.doc);
        final TextView dop = dialog.findViewById(R.id.dop);
        final Button docBtn = dialog.findViewById(R.id.docBtn);
        final Button dopBtn = dialog.findViewById(R.id.dopBtn);
        final Button proceedBtn = dialog.findViewById(R.id.proceedBtn);
        final Button rdsoBtn = dialog.findViewById(R.id.rdsoBtn);
        imgTaggingDialog = dialog.findViewById(R.id.img);
        final Spinner spinner = dialog.findViewById(R.id.productSpinner);
        final ProgressBar progressBar = dialog.findViewById(R.id.progressBar);

        head.setText(actionType);

        docBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //DatePickerUtility.getDob(CheckInScanActivity.this,null,doc);
            }
        });
        dopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //DatePickerUtility.getDob(CheckInScanActivity.this,null,dop);
            }
        });
        rdsoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera(REQUEST_IMAGE_CAPTURE_RDSO_CERTIFICATE);
            }
        });

        //productCOuntTxt.setText("Total : "+productList.size()+" QR products");
       // getDivision(progressBar,spinner);

        proceedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                //uploadDataToServer();
                //CommonUiItems.myDialog(CheckInScanActivity.this,"Congratulations!!","Your data uploaded successfully.","ok",null).show();
            }
        });
    }
    /*private void uploadDataToServer(){


            ApiInterface apiInterface = ApiClient.api;
            for(CheckinRequest c: checkinRequests){
                Call<CheckinResponse> checkinResponseCall = apiInterface.checkinQRCode(c);
                checkinResponseCall.enqueue(new Callback<CheckinResponse>() {
                    @Override
                    public void onResponse(Call<CheckinResponse> call, Response<CheckinResponse> response) {
                        Log.d("Response", response.code() + " " + response.message());
                        if (response.body().getStatus().equals("1")) {
                            Log.d("Success", response.body().getMessage());
                            Toast.makeText(CheckInScanActivity.this, "Data uploaded to server successfully", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CheckInScanActivity.this, "nothing", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CheckinResponse> call, Throwable t) {
                        Toast.makeText(CheckInScanActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

        }*/
    /*private void getDivision(final ProgressBar progressBar, final Spinner spinner){
        progressBar.setVisibility(View.VISIBLE);
        Log.d("getDivision","invoked");
        ApiClient.api.getDivision(new GetDivisionReq()).enqueue(new Callback<GetDivisionRes>() {
            @Override
            public void onResponse(Call<GetDivisionRes> call, Response<GetDivisionRes> response) {
                GetDivisionRes res = response.body();
                divisionList = res.getData().get(0).getDivision();
                divisionList.add(0,new Division(){
                    @NonNull
                    @Override
                    public String toString() {
                        return "-----Select Division-----";
                    }
                });
                ArrayAdapter<Division> adapter = new ArrayAdapter<Division>(CheckInScanActivity.this, R.layout.spinner_item,divisionList);
                spinner.setAdapter(adapter);
                progressBar.setVisibility(View.GONE);
                Log.d("getDivision",divisionList.toString());
                Toast.makeText(CheckInScanActivity.this, "List fetched", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GetDivisionRes> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.d("getDivision","error : "+t.getMessage());
                CommonUiItems.myDialog(CheckInScanActivity.this,"error",t.getMessage(),"ok",null);
            }
        });
    }*/


    public void openCamera(int rc){
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, rc);
            }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE_RDSO_CERTIFICATE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imgTaggingDialog.setImageBitmap(imageBitmap);
           // imageString = ImageUtils.convert(imageBitmap);
        }
    }

    private void submitTaggingData(){

    }
}
