package org.genuinemark.supplybeam.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import org.genuinemark.supplybeam.base.BaseActivity
import org.genuinemark.supplybeam.R
import org.genuinemark.supplybeam.ui.activities.DashboardActivity
import org.genuinemark.supplybeam.ui.activities.OtpVerifyActivity
import org.genuinemark.supplybeam.utils.SystemUtility


class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({

            if (!SystemUtility.getId(applicationContext).equals("") ){
                startActivity(
                    Intent(
                        this,
                        DashboardActivity::class.java
                    ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                )

            } else {
                startActivity(
                    Intent(
                        this,
                        OtpVerifyActivity::class.java
                    ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                )
            }





            /*if (!getBooleanDataFromPreferences(Constants.PREFERENCE_KEY_IS_LOGGED_IN)) {
                startActivity(Intent(this, LoginActivity::class.java))
            } else {
                val userRole = getStringDataFromPreferences(Constants.PREFERENCE_KEY_USER_TYPE)
                val userModel = Gson().fromJson(
                    getStringDataFromPreferences(Constants.PREFERENCE_KEY_USER_DATA),
                    UserModel::class.java
                )
                if (Constants.ROLE_CBP.equals(userRole,true) && TextUtils.isEmpty(userModel.cbp_role)) {
                    startActivity(
                        Intent(
                            this,
                            AddCBPPersonActivity::class.java
                        ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    )
                } else if (intent.extras != null) {
                    val notificationData =
                        NotificationData(
                            intent.extras.get("receiver_id") as String?,
                            intent.extras.get(Constants.NOTIFICATION_TYPE) as String?,
                            intent.extras.get("body") as String?,
                            intent.extras.get("title") as String?,
                            intent.extras.get("user_name") as String?,
                            intent.extras.get("sender") as String?,
                            intent.extras.get("group_id") as String?,
                            intent.extras.get("group_name") as String?,
                            intent.extras.get("chat_type") as String?
                        )

                    startActivity(
                        Intent(
                            this,
                            MainActivity::class.java
                        ).putExtra(
                            Constants.NOTIFICATION_TYPE,
                            intent.getStringExtra(Constants.NOTIFICATION_TYPE)
                        ).putExtra(Constants.NOTIFICATION_DATA, notificationData)
                    )
                } else {
                    startActivity(Intent(this, MainActivity::class.java))
                }
            }*/

            finish()
        }, 1500)
    }

    override fun onStart() {
        super.onStart()

    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        this.intent = intent
    }


}
