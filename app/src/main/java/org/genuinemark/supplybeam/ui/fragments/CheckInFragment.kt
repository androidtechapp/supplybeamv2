package org.genuinemark.supplybeam.ui.fragments


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.header_layout.view.*
import org.genuinemark.supplybeam.R
import org.genuinemark.supplybeam.adapter.CheckInAdapter
import org.genuinemark.supplybeam.api.input.CheckInInput
import org.genuinemark.supplybeam.api.response.JsonObjectResponse
import org.genuinemark.supplybeam.api.response.Resource
import org.genuinemark.supplybeam.api.response.Status
import org.genuinemark.supplybeam.base.BaseFragment
import org.genuinemark.supplybeam.databinding.FragmentCheckInBinding
import org.genuinemark.supplybeam.models.CheckInModel
import org.genuinemark.supplybeam.ui.activities.CheckInBarCodeScanActivity
import org.genuinemark.supplybeam.utils.OnItemClickListener
import org.genuinemark.supplybeam.utils.SystemUtility
import org.genuinemark.supplybeam.viewModel.HomeViewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass.
 */
class CheckInFragment : BaseFragment(), OnItemClickListener<CheckInModel.CheckInData> {

     lateinit var responseList: ArrayList<CheckInModel.CheckInData>
    lateinit var binding: FragmentCheckInBinding
    lateinit var homeViewModel: HomeViewModel
    //  lateinit var adapter: RecyclerViewArrayAdapter<CheckInModel.CheckInData>

    private val adapter by lazy {
        CheckInAdapter(mutableListOf(), activity)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCheckInBinding.inflate(inflater, container, false).apply { }

        init()
        return binding.root
    }

    fun init() {
        binding.headerLayout.ivBackBtn.setOnClickListener(this)
        binding.ivCheckInAll.setOnClickListener(this)
        binding.cbSelectAll.setOnClickListener(this)
        binding.headerLayout.visibility = View.VISIBLE
        binding.headerLayout.ivTxtHeading.setText(getString(R.string.check_in))

        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)


        val sdf = SimpleDateFormat("EEE, MMM d", Locale.getDefault())
        val currentDateandTime: String = sdf.format(Date())
        binding.tvDateTime.text= currentDateandTime + " | 80f"
        callCheckInApi()
        observeViewModel(homeViewModel)
    }


    private fun callCheckInApi() {
        val checkInInput = CheckInInput(
            "31"
        )
        if (SystemUtility.isNetworkAvailable(activity!!)) {
            homeViewModel.setCheckInInput(checkInInput)

        } else {
            onNoInternet()
        }

    }


    private fun observeViewModel(homeViewModel: HomeViewModel) {
        try {

            /**
             * Call Observable for chekc-in
             */
            homeViewModel.getCheckInObservable()?.observe(viewLifecycleOwner,
                Observer<Resource<JsonObjectResponse<CheckInModel>>> { response ->
                    if (response != null) {
                        if (response.headers != null) {
                            processHeaders(response.headers)
                        }
                        when {
                            response.status == Status.LOADING -> {
                                enableLoadingBar(true)
                            }

                            response.status == Status.SUCCESS -> {
                                enableLoadingBar(false)
                                setAdapter(response.data?.`object`?.itemList)
                            }

                            response.status == Status.ERROR -> {
                                enableLoadingBar(false)
                            }

                            response.status == Status.FAILURE -> {
                                enableLoadingBar(false)
                            }

                            response.status == Status.FORCE_UPDATE -> {

                            }
                        }
                    }
                })

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setAdapter(itemList: ArrayList<CheckInModel.CheckInData>?) {
        responseList=itemList!!
        try {

            binding.checkInsRecyclerView.layoutManager =
                LinearLayoutManager(context) as RecyclerView.LayoutManager
            binding.checkInsRecyclerView.adapter = adapter

            val dividerItemDecoration = DividerItemDecoration(
                binding.checkInsRecyclerView.context,
                LinearLayoutManager.VERTICAL
            )
            binding.checkInsRecyclerView.addItemDecoration(dividerItemDecoration)
            adapter.addData(responseList)

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun onItemClick(view: View, `object`: CheckInModel.CheckInData, position: Int) {

    }


    override fun onClick(v: View?) {
        super.onClick(v)
        when (v?.id) {

            R.id.ivBackBtn -> {
                requireActivity().onBackPressed()
            }


            R.id.ivCheckInAll -> {


                startActivity(Intent(context, CheckInBarCodeScanActivity::class.java))
               // responseList.get(0).id

             // findNavController().navigate(R.id.checkInOutFragment)
            }
            R.id.cbSelectAll->{
                //select all adapter box

                adapter.selectAllCheckBox(binding.cbSelectAll?.isChecked)
            }

        }
    }

}