package org.genuinemark.supplybeam.ui.activities

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import org.genuinemark.supplybeam.R
import org.genuinemark.supplybeam.api.input.LoginInput
import org.genuinemark.supplybeam.api.input.OTPInput
import org.genuinemark.supplybeam.api.response.JsonObjectResponse
import org.genuinemark.supplybeam.api.response.Resource
import org.genuinemark.supplybeam.api.response.Status
import org.genuinemark.supplybeam.base.BaseActivity
import org.genuinemark.supplybeam.databinding.ActivityOtpverifyBinding
import org.genuinemark.supplybeam.model.UserModel
import org.genuinemark.supplybeam.models.SendOtpModel
import org.genuinemark.supplybeam.utils.StringUtility
import org.genuinemark.supplybeam.utils.SystemUtility
import org.genuinemark.supplybeam.viewModel.OtpVerifyViewModel
import android.R.string.cancel
import androidx.databinding.adapters.TextViewBindingAdapter.setText
import android.widget.TextView
import android.view.Gravity
import android.R.attr.gravity
import android.view.WindowManager
import android.view.Window.FEATURE_NO_TITLE
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import android.app.Dialog
import android.view.Window
import android.widget.Button


class OtpVerifyActivity: BaseActivity() {

    lateinit var binding: ActivityOtpverifyBinding
    lateinit var otpVerifyViewModel: OtpVerifyViewModel
    var isValidateMsg:Boolean= false
    internal var tryAgainDialog: AlertDialog? = null
    val MY_PERMISSIONS_REQUEST_SMS_RECEIVE: Int = 10
    //smsReceiver: SmsReceiver  = new SmsReceiver();

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, org.genuinemark.supplybeam.R.layout.activity_otpverify)

        init()

    }

    fun init(){
        otpVerifyViewModel = ViewModelProvider(this).get(OtpVerifyViewModel::class.java)
        binding.ivMobileNumber.addTextChangedListener(GenericTextWatcher(binding.ivOtp))
        observeViewModel(otpVerifyViewModel)
    }

    private fun observeViewModel(otpVerifyViewModel: OtpVerifyViewModel) {
        try{

            otpVerifyViewModel.getUserLoginObservable()?.observe(this,
                Observer<Resource<JsonObjectResponse<SendOtpModel>>> { response->
                    if (response != null){
                        if (response.headers!= null){
                            processHeaders(response.headers)
                        }
                        when{
                            response.status == Status.LOADING ->{
                                enableLoadingBar(true)
                            }

                            response.status == Status.SUCCESS ->{

                                toast("Otp sent to your mobile number")
                                hideKeyBoard()
                                timer.start()
                                enableLoadingBar(false)
                                isValidateMsg = true  // true means we set validation msg for OTP otherwise for mobile number
                            }

                            response.status == Status.ERROR ->{
                                enableLoadingBar(false)
                            }

                            response.status == Status.FAILURE ->{
                                enableLoadingBar(false)
                            }

                            response.status == Status.FORCE_UPDATE ->{

                            }
                        }
                    }
                })


            otpVerifyViewModel.getOtpVerifyObservable()?.observe(this,
                Observer<Resource<JsonObjectResponse<UserModel>>> { response->
                    if (response != null){
                        if (response.headers!= null){
                            processHeaders(response.headers)
                        }
                        when{
                            response.status == Status.LOADING ->{
                                enableLoadingBar(true)
                            }

                            response.status == Status.SUCCESS ->{
                                enableLoadingBar(false)
                                toast(response.message)
                                saveAndRedirectResponse(response)
                            }

                            response.status == Status.ERROR ->{
                                enableLoadingBar(false)
                                toast(response.message)
                            }

                            response.status == Status.FAILURE ->{
                                enableLoadingBar(false)
                                toast(response.message)
                            }

                            response.status == Status.FORCE_UPDATE ->{

                            }
                        }
                    }
                })

        }catch (e: Exception){
            toast(e.printStackTrace().toString())
            toast(e.message)
            e.printStackTrace()
        }
    }



    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){

            R.id.ivSignIn->{
                if(validateAll()) {

                    timer.cancel()
                    val loginInput = LoginInput(
                        binding.ivOtp.text.toString(),
                        binding.ivMobileNumber.text.toString()
                    )
                    if (SystemUtility.isNetworkAvailable(getApplicationContext())){
                        otpVerifyViewModel.setOtpVerify(loginInput)
                        hideKeyBoard()
                    } else{
                        onNoInternet()
                    }
                    /*startActivity(
                        Intent(
                            this,
                            DashboardActivity::class.java
                        ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    )*/

                }
            }
        }
    }

    private fun validateAll(): Boolean {
        if (!StringUtility.validateEditText(binding.ivOtp)){
            if (isValidateMsg) {
                toast(getString(R.string.valid_otp))

            } else {
                toast(getString(R.string.mobile_digit))
            }
            return false
        }

        return true
    }

    inner class GenericTextWatcher public constructor(private val view: View) : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            val text:String  = editable.toString()
            if(text.length == 10){

                val otpInput = OTPInput(
                    text
                )
                if (SystemUtility.isNetworkAvailable(getApplicationContext())){
                    otpVerifyViewModel.setOtpInput(otpInput)
                } else{
                    onNoInternet()
                }

            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

    }


    internal var timer: CountDownTimer = object : CountDownTimer(60000, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            binding.timerTextSignIn.setText("seconds remaining for OTP : ")
            binding.timerSignIn.setText("" + millisUntilFinished / 1000)
            if (binding.timerSignIn.getVisibility() == View.VISIBLE) {
                binding.textViewOTPSignIn.setVisibility(View.INVISIBLE)
                binding.timerTextSignIn.setVisibility(View.VISIBLE)
            } else {
                binding.textViewOTPSignIn.setVisibility(View.VISIBLE)
                binding.timerTextSignIn.setVisibility(View.INVISIBLE)
            }

        }

        override fun onFinish() {
            try {
                //showTryAgainDialog()
                showDialog()
                binding.timerTextSignIn.setText("please try again after some time.")
            } catch (e: Exception) {
               // Crashlytics.log("showTryAgainDialog causing error : $e")
            }

            //            textViewOTPSignIn.setVisibility(View.VISIBLE);
            //            timerTextSignIn.setVisibility(View.INVISIBLE);
            //            timerSignIn.setVisibility(View.INVISIBLE);
        }
    }

    private fun showDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.support_dialog)

        dialog.getWindow()?.getDecorView()?.setBackgroundResource(android.R.color.transparent)

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.getWindow()?.getAttributes())
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.getWindow()?.setAttributes(lp)

        val ok = dialog.findViewById(R.id.ok) as Button
        val tvTitle = dialog.findViewById(R.id.tvTitle) as TextView
        tvTitle.text = getString(R.string.resend_otp)
        val cancel = dialog.findViewById(R.id.cancel) as Button

        ok.setOnClickListener(View.OnClickListener {
            val otpInput = OTPInput(
                binding.ivMobileNumber.getText().toString()
            )
            if (SystemUtility.isNetworkAvailable(getApplicationContext())){
                otpVerifyViewModel.setOtpInput(otpInput)
            } else{
                onNoInternet()
            }
            dialog.dismiss()
            //finishAffinity()
        })


        cancel.setOnClickListener(View.OnClickListener { dialog.dismiss() })

        dialog.show()
    }

    private fun showTryAgainDialog() {
        val builder = AlertDialog.Builder(this@OtpVerifyActivity)  //, R.style.AlertDialogCustom
            .setTitle("Didn't get OTP ?")
            .setMessage("If you didn't receive an OTP, click on try again.")
            .setPositiveButton(
                "Wait"
            ) { dialogInterface, i -> }
            .setNegativeButton(
                "Try Again"
            ) { dialogInterface, i ->
                if (binding.ivMobileNumber.getText().toString().trim({ it <= ' ' }).length == 10) {

                    val otpInput = OTPInput(
                        binding.ivMobileNumber.getText().toString()
                    )
                    if (SystemUtility.isNetworkAvailable(getApplicationContext())){
                        otpVerifyViewModel.setOtpInput(otpInput)
                    } else{
                        onNoInternet()
                    }
                    //resendOtp(mobile.getText().toString().trim({ it <= ' ' }))

                } else {
                    Toast.makeText(
                        this@OtpVerifyActivity,
                        getString(R.string.resent_otp_validation),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        tryAgainDialog = builder.create()
        tryAgainDialog?.show()
    }

    override fun onPause() {
        super.onPause()
        if (tryAgainDialog != null) {
            tryAgainDialog?.dismiss()
            tryAgainDialog = null
        }
    }

    private fun checkAndRequestPermissions(): Boolean {
        val sms = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS)
        if (sms != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.RECEIVE_SMS),
                MY_PERMISSIONS_REQUEST_SMS_RECEIVE
            )
            return false
        }
        return true
    }



    /*override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_SMS_RECEIVE) {
            // YES!!
            // smsRetreive();
            smsReceiver.bindListener(smsListener);
            //  Log.i("TAG", "MY_PERMISSIONS_REQUEST_SMS_RECEIVE --> YES");
        }
    }*/


}