package org.genuinemark.supplybeam.ui.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.dynamicanimation.animation.SpringAnimation
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_home.*
import org.genuinemark.supplybeam.R
import org.genuinemark.supplybeam.adapter.SliderAdapter
import org.genuinemark.supplybeam.api.input.DashboardInput
import org.genuinemark.supplybeam.api.input.LoginInput
import org.genuinemark.supplybeam.api.response.JsonObjectResponse
import org.genuinemark.supplybeam.api.response.Resource
import org.genuinemark.supplybeam.api.response.Status
import org.genuinemark.supplybeam.base.BaseFragment
import org.genuinemark.supplybeam.databinding.FragmentHomeBinding
import org.genuinemark.supplybeam.models.DashboardModel
import org.genuinemark.supplybeam.models.SendOtpModel
import org.genuinemark.supplybeam.util.listenForAllSpringsEnd
import org.genuinemark.supplybeam.util.spring
import org.genuinemark.supplybeam.utils.SystemUtility
import org.genuinemark.supplybeam.viewModel.HomeViewModel
import org.genuinemark.supplybeam.viewModel.OtpVerifyViewModel

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : BaseFragment() {

    lateinit var binding: FragmentHomeBinding
    lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false).apply {  }

        init()

        setUpViewPager()

        return binding.root
    }

    fun init(){
        binding.txtCheckIn.setOnClickListener(this)
        binding.txtCheckInPrdtTag.setOnClickListener(this)
        binding.txtRdsoCertification.setOnClickListener(this)
        binding.txtRdsoCertification.setOnClickListener(this)
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        callApi()
        observeViewModel(homeViewModel)
    }

    private fun callApi() {
        val dashboardInput = DashboardInput(
            SystemUtility.getId(activity!!)//"2"
        )
        if (SystemUtility.isNetworkAvailable(activity!!)){
            homeViewModel.setOtpInput(dashboardInput)

        } else{
            onNoInternet()
        }
    }

    private fun observeViewModel(homeViewModel: HomeViewModel) {
        try{
            homeViewModel.getUserInfoObservable()?.observe(viewLifecycleOwner,
                Observer<Resource<JsonObjectResponse<DashboardModel>>> { response->
                    if (response != null){
                        if (response.headers!= null){
                            processHeaders(response.headers)
                        }
                        when{
                            response.status == Status.LOADING ->{
                                enableLoadingBar(true)
                            }

                            response.status == Status.SUCCESS ->{
                                enableLoadingBar(false)
                                updateUi(response)
                            }

                            response.status == Status.ERROR ->{
                                enableLoadingBar(false)
                            }

                            response.status == Status.FAILURE ->{
                                enableLoadingBar(false)
                            }

                            response.status == Status.FORCE_UPDATE ->{

                            }
                        }
                    }
                })

        } catch (e: Exception){
            e.printStackTrace()
        }
    }

    private fun updateUi(response: Resource<JsonObjectResponse<DashboardModel>>?) {
        try{

            if (response?.data?.`object`?.permission?.checkin == true){
                binding.txtCheckIn.visibility = View.VISIBLE
            }

            if (response?.data?.`object`?.permission?.checkinProductTagging == true){
                binding.txtCheckInPrdtTag.visibility = View.VISIBLE
            }

            if (response?.data?.`object`?.permission?.rdsoCertification == true){
                binding.txtRdsoCertification.visibility = View.VISIBLE
            }

            if (response?.data?.`object`?.permission?.rejectionCheckout == true){
                binding.txtRejectionCheckout.visibility = View.VISIBLE
            }

            if (response?.data?.`object`?.permission?.checkout == true){
                binding.txtCheckOut.visibility = View.VISIBLE
            }

            if (response?.data?.`object`?.totalChckOut != null && !response?.data?.`object`?.totalChckOut.equals("")){
                ivCheckOutTxt.setText(response?.data?.`object`?.totalChckOut)
            }

            if (response?.data?.`object`?.totalInHand!= null && !response?.data?.`object`?.totalInHand.equals("")){
                ivInHand.setText(response?.data?.`object`?.totalInHand)
            }

            if (response?.data?.`object`?.totalInTransit!= null && !response?.data?.`object`?.totalInTransit.equals("")){
                ivInTransit.setText(response?.data?.`object`?.totalInTransit)
            }

            if (response?.data?.`object`?.bannerImage!= null && !response?.data?.`object`?.bannerImage.equals("")){
                Glide.with(activity!!)  //2
                    .load(response?.data?.`object`?.bannerImage) //3
                    .centerCrop() //4
                    //.placeholder(R.drawable.ic_image_place_holder) //5
                    .error(R.drawable.ic_launcher_background) //6
                    //.fallback(R.drawable.ic_no_image) //7
                    .into(binding.sliderImage) //
            }

            if (response?.data?.`object`?.totalChckOut!= null && !response?.data?.`object`?.totalChckOut.equals("")){
                binding.ivCheckOutTxt.setText(response?.data?.`object`?.totalChckOut)
            }

            if (response?.data?.`object`?.totalInHand!= null && !response?.data?.`object`?.totalInHand.equals("")){
                binding.ivInHand.setText(response?.data?.`object`?.totalInHand)
            }

            if (response?.data?.`object`?.totalInTransit!= null && !response?.data?.`object`?.totalInTransit.equals("")){
                binding.ivInTransit.setText(response?.data?.`object`?.totalInTransit)
            }

        } catch (e: Exception){
            e.printStackTrace()
        }
    }


    private fun setUpViewPager() {
        SliderAdapter().apply {
            submitList(centeredImageResources)
        }.also { binding.introScreenViewPager.adapter = it }

        binding.wormIndicator.setViewPager2(binding.introScreenViewPager)
        binding.introScreenViewPager.registerOnPageChangeCallback(viewPagerOnChangeCallback)
    }

    private val viewPagerOnChangeCallback= object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            binding.sliderImage.spring(SpringAnimation.ALPHA).apply {
                animateToFinalPosition(0f)
            }.also { springAnimation ->
                listenForAllSpringsEnd({
                    springAnimation.animateToFinalPosition(1f)
                    binding.sliderImage.setImageResource(centeredImageResources[position])
                }, springAnimation)
            }
        }
    }

    private companion object {
        private val centeredImageResources = mutableListOf(
            R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_background
        )
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onClick(v: View?) {
        super.onClick(v)
        when(v?.id){

            R.id.txtCheckIn->{
                findNavController().navigate(R.id.action_homeFragment_to_checkInFragment)
            }

            R.id.txtCheckInPrdtTag->{
                findNavController().navigate(R.id.action_homeFragment_to_checkInFragment)
            }

            R.id.txtRdsoCertification->{
                findNavController().navigate(R.id.action_homeFragment_to_checkInFragment)
            }

            R.id.txtRdsoCertification->{
                findNavController().navigate(R.id.action_homeFragment_to_checkInFragment)
            }
        }
    }
}
