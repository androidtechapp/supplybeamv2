package org.genuinemark.supplybeam.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import org.genuinemark.supplybeam.databinding.CheckinItemBinding
import org.genuinemark.supplybeam.models.CheckInModel
import org.genuinemark.supplybeam.ui.activities.CheckInBarCodeScanActivity
import org.genuinemark.supplybeam.ui.fragments.CheckInFragment
import org.genuinemark.supplybeam.ui.fragments.CheckInOutFragment

class CheckInAdapter(
    private var items: MutableList<CheckInModel.CheckInData>,
    private var act: Activity?

) : RecyclerView.Adapter<CheckInAdapter.SubMasterViewHolder>() {

    var selectAll= MutableLiveData<Boolean>()

    fun addData(itemsList: ArrayList<CheckInModel.CheckInData>?) {
        this.items=itemsList!!.toMutableList()
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubMasterViewHolder {
        return SubMasterViewHolder(
            CheckinItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }
    override fun getItemCount(): Int {
            return items.size
    }
    override fun onBindViewHolder(holder: SubMasterViewHolder, position: Int) {
        holder.bind(position)

    }

    fun selectAllCheckBox(isSelectted:Boolean) {
//       items.forEachIndexed { index, checkInData -> checkInData.isSelected=isSelectted  }

       var updatedList= items
        for (list in updatedList)
        {
            list.isSelected= isSelectted
        }
        items=updatedList
        notifyDataSetChanged()
    }

    inner class SubMasterViewHolder constructor(val binding: CheckinItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(position: Int) {
            binding.item = items[position]
            binding.executePendingBindings()

                binding.cartoonCb.isChecked= items[position]!!.isSelected!!
            binding.cartoonCb.setOnClickListener {
                if(binding.cartoonCb.isChecked)
                act?.startActivity(Intent(act, CheckInBarCodeScanActivity::class.java).putExtra("id",items[position].id))
            }

        }
    }
}