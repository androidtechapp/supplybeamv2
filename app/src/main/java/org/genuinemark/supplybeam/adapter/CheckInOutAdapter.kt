package org.genuinemark.supplybeam.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import org.genuinemark.supplybeam.R
import org.genuinemark.supplybeam.databinding.CheckinListItemBinding
import org.genuinemark.supplybeam.models.CheckInOutModel
import org.genuinemark.supplybeam.utils.CommonUiItems

class CheckInOutAdapter(
    private var items: MutableList<CheckInOutModel.CheckInOutData>,
    private var act: Activity?

) : RecyclerView.Adapter<CheckInOutAdapter.SubMasterViewHolder>() {

    var selectAll= MutableLiveData<Boolean>()

    fun addData(item: CheckInOutModel.CheckInOutData) {
        this.items.add(item)
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubMasterViewHolder {
        return SubMasterViewHolder(
            CheckinListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }
    override fun getItemCount(): Int {
            return items.size
    }
    override fun onBindViewHolder(holder: SubMasterViewHolder, position: Int) {
        holder.bind(position)

    }


    inner class SubMasterViewHolder constructor(val binding: CheckinListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(position: Int) {
            binding.item = items[position]
            binding.executePendingBindings()
            binding.txtCartoon.setOnClickListener {
                val dialog =
                    CommonUiItems.getCustomDialog(act, R.layout.dialog_checkinout_more)
                    dialog.show()

                  dialog.findViewById<TextView>(R.id.btnRemove).setOnClickListener {
                      items.removeAt(position)
                      notifyDataSetChanged()

                  }
            }

        }
    }
}