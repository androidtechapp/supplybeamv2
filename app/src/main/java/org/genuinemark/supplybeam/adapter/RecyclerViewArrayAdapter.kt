package org.genuinemark.supplybeam.adapters

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import org.genuinemark.supplybeam.BR
import org.genuinemark.supplybeam.R
import org.genuinemark.supplybeam.base.BaseModel
import org.genuinemark.supplybeam.models.CheckInModel
import org.genuinemark.supplybeam.utils.OnItemClickListener
import org.genuinemark.supplybeam.utils.OnItemLongClickListener


/*This is the adapter class for all the RecyclerView*/
class RecyclerViewArrayAdapter<T : BaseModel>(
    private var mList: ArrayList<T>?,
    onItemClickListener: OnItemClickListener<T>
) : RecyclerView.Adapter<RecyclerViewArrayAdapter.MyViewHolder>() {

    private val onItemClickListener: OnItemClickListener<T>?
    private var onItemLongClickListener: OnItemLongClickListener<T>? = null
    private var emptyTextView: TextView? = null
    private var emptyViewText = R.string.no_record_found
    internal var fragment: Fragment? = null
    internal var activity: Activity? = null
    internal lateinit var context: Context
    var userId: String? = ""
    var conversationType: String? = ""

    init {
        this.onItemClickListener = onItemClickListener
    }

    fun setLongItemClickListener(onItemLongClickListener: OnItemLongClickListener<T>) {
        this.onItemLongClickListener = onItemLongClickListener
    }


    fun setEmptyTextView(emptyTextView: TextView, @StringRes emptyViewText: Int) {
        this.emptyTextView = emptyTextView
        this.emptyViewText = emptyViewText
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: ViewDataBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), viewType, parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.setVariable(BR.item, getItem(position))
        holder.binding.setVariable(BR.itemClickListener, onItemClickListener)
        holder.binding.setVariable(BR.position, position)
    }

    /* return layout id to be inflated for given position
     * */
    override fun getItemViewType(position: Int): Int {
        if (getItem(position) is CheckInModel.CheckInData ){
            return R.layout.item_list
        } else if (position == 1) {
            return R.layout.item_list
        } else {
            return -1
        }
    }

    fun getItem(position: Int): T {
        return mList!![position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun setSelectedItems(list: ArrayList<T>) {
        for (mObject in this.mList!!) {
            mObject.setSelected(list.contains(mObject))

        }
    }

    fun setSearchList(list: ArrayList<T>) {
        this.mList = list
    }

    fun add(`object`: T) {
        mList!!.add(`object`)
        notifyItemInserted(itemCount - 1)
    }

    fun addAll(objects: ArrayList<T>?) {
        val posStart = mList!!.size
        mList!!.addAll(objects!!)
        notifyItemRangeChanged(posStart, objects.size)
    }

    fun onNewMessage(msg: T?) {
        mList!!.add(0, msg!!)
        notifyItemInserted(0)
    }


    fun setLoggedInUserId(userId: String?) {
        this.userId = userId
    }


    fun setChatType(chatType: String?) {
        this.conversationType = chatType
    }

    fun getSelectedItemList(): ArrayList<T> {
        var list = ArrayList<T>()
        for (item in mList!!) {
            if (item.isSelected()) {
                list.add(item)
            }
        }
        return list
    }

    override fun getItemCount(): Int {
        if (emptyTextView != null) {
            if (mList!!.isEmpty()) {
                emptyTextView!!.visibility = View.VISIBLE
                emptyTextView!!.setText(emptyViewText)
            } else {
                emptyTextView!!.visibility = View.GONE
            }
        }
        return mList!!.size
    }

    class MyViewHolder(binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

        var binding: ViewDataBinding
            internal set

        init {
            this.binding = binding
            this.binding.executePendingBindings()
        }
    }
}
