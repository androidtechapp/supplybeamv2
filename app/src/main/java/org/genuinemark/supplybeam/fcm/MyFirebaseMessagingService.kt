package org.genuinemark.supplybeam.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import org.genuinemark.supplybeam.R
import org.genuinemark.supplybeam.activities.SplashActivity
import org.genuinemark.supplybeam.utils.Constants
import org.genuinemark.supplybeam.utils.SharedPreferenceUtils
import com.google.firebase.messaging.FirebaseMessagingService



class MyFirebaseMessagingService : FirebaseMessagingService() {
    internal var channelName = "yogous_notifications"

    override fun onCreate() {
        super.onCreate()

    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        SharedPreferenceUtils.saveFCMToken(this, token)
    }
/*    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if (remoteMessage.getData().size > 0) {
            Log.e("Data ", "Payload: " + remoteMessage.getData().toString())
            handleNotification(remoteMessage.getData(), remoteMessage.getNotification()!!.getBody())
            return
        }
        if (remoteMessage.getNotification() != null) {
            val notificationPayload = JSONObject(remoteMessage.data)

            generateNotification(
                notificationPayload.getString(NOTIFICATION_TITLE),
                remoteMessage.getNotification()!!.getBody()
            )
        }
    }

    private fun handleNotification(jsonObject: Map<String, String>, body: String?) {
        try {
            val notificationType = jsonObject.get(Constants.NOTIFICATION_TYPE)
            val notificationPayload = JSONObject(jsonObject)
            val pushNotification = Intent(this, MainActivity::class.java)
            var targetId: String? = ""
            pushNotification.putExtra(
                Constants.NOTIFICATION_TYPE,
                notificationType
            )
            try {
                val notificationData = Gson().fromJson<NotificationData>(
                    notificationPayload.toString(),
                    NotificationData::class.java
                )
                if (!TextUtils.isEmpty(notificationData.group_id)) {
                    targetId = notificationData.group_id
                } else {
                    try {
                        targetId = JSONObject(notificationData!!.sender).getString("_id")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
                pushNotification.putExtra(
                    Constants.NOTIFICATION_DATA,
                    notificationData
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (Constants.ACTIVITY_CHAT_MESSAGE.equals(
                    notificationType,
                    true
                ) && Yogous.currentActivity is ChatActivity
            ) {
                if (!(Yogous.currentActivity as ChatActivity).targetId.equals(targetId)) {
                    generateNotification(
                        notificationPayload.getString(NOTIFICATION_TITLE),
                        body,
                        pushNotification
                    )
                }
            } else {
                generateNotification(
                    notificationPayload.getString(NOTIFICATION_TITLE),
                    body,
                    pushNotification
                )
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun generateNotification(title: String, messageBody: String?, pushNotification: Intent) {
        pushNotification.putExtra(Constants.NAVIGATE_FROM, 1)
        pushNotification.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val pendingIntent =
            PendingIntent.getActivity(
                this,
                System.currentTimeMillis().toInt(),
                pushNotification,
                PendingIntent.FLAG_ONE_SHOT
            )
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder =
            NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                .setContentTitle(title)
                .setContentText(messageBody)
                .setStyle(NotificationCompat.BigTextStyle().bigText(messageBody))
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_notification)
                .setSound(defaultSoundUri)
                .setColor(ContextCompat.getColor(this, R.color.colorAccent))
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_MAX)
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                getString(R.string.default_notification_channel_id),
                channelName,
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(System.currentTimeMillis().toInt(), notificationBuilder.build())
    }*/

    fun generateNotification(title: String, messageBody: String?) {
        val intent = Intent(this, SplashActivity::class.java).putExtra(Constants.NAVIGATE_FROM, 1)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this,
            System.currentTimeMillis().toInt(),
            intent,
            PendingIntent.FLAG_ONE_SHOT
        )
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder =
            NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setStyle(NotificationCompat.BigTextStyle().bigText(messageBody))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setColor(ContextCompat.getColor(this, R.color.colorAccent))
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_MAX)
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                getString(R.string.default_notification_channel_id),
                channelName,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(System.currentTimeMillis().toInt(), notificationBuilder.build())
    }
}