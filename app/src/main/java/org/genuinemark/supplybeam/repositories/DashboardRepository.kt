package org.genuinemark.supplybeam.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.genuinemark.supplybeam.api.input.CheckInInput
import org.genuinemark.supplybeam.api.input.CheckInOutInput
import org.genuinemark.supplybeam.api.input.DashboardInput
import org.genuinemark.supplybeam.api.input.OTPInput
import org.genuinemark.supplybeam.api.response.ApiService
import org.genuinemark.supplybeam.api.response.JsonObjectResponse
import org.genuinemark.supplybeam.api.response.Resource
import org.genuinemark.supplybeam.api.response.Status
import org.genuinemark.supplybeam.models.CheckInModel
import org.genuinemark.supplybeam.models.CheckInOutModel
import org.genuinemark.supplybeam.models.DashboardModel
import org.genuinemark.supplybeam.models.SendOtpModel
import org.genuinemark.supplybeam.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Singleton

@Singleton
class DashboardRepository constructor(var apiService: ApiService) {

    /**
     * API for Dashboard Info
     */
    fun userInfo(dashboardInput: DashboardInput): LiveData<Resource<JsonObjectResponse<DashboardModel>>> {

        val data = MutableLiveData<Resource<JsonObjectResponse<DashboardModel>>>()

        data.value = Resource(Status.LOADING, null, "")

        apiService.userInfo(dashboardInput).enqueue(object : Callback<JsonObjectResponse<DashboardModel>> {
            override fun onResponse(
                call: Call<JsonObjectResponse<DashboardModel>>,
                response: Response<JsonObjectResponse<DashboardModel>>
            ) {
                if (response.body()?.status.equals(Constants.SUCCESS) /*&& response.body()?.code.equals(
                        Constants.SUCCESS_CODE
                    )*/
                ) {
                    data.value = Resource(
                        Status.SUCCESS,
                        response.body(),
                        response.body()?.msg,
                        response.headers()
                    )
                } else {
                    data.value = Resource(
                        Status.ERROR,
                        response.body(),
                        response.body()?.msg,
                        response.headers()
                    )
                }
            }

            override fun onFailure(call: Call<JsonObjectResponse<DashboardModel>>, t: Throwable) {
                data.value = Resource(Status.FAILURE, null, t.localizedMessage)
            }
        })

        return data
    }



    /**
     * API for CheckIn Info
     */
    fun checkInInfo(checkInInput: CheckInInput): LiveData<Resource<JsonObjectResponse<CheckInModel>>> {

        val data = MutableLiveData<Resource<JsonObjectResponse<CheckInModel>>>()

        data.value = Resource(Status.LOADING, null, "")

        apiService.checkInInfo(checkInInput).enqueue(object : Callback<JsonObjectResponse<CheckInModel>> {
            override fun onResponse(
                call: Call<JsonObjectResponse<CheckInModel>>,
                response: Response<JsonObjectResponse<CheckInModel>>
            ) {
                if (response.body()?.status.equals(Constants.SUCCESS) /*&& response.body()?.code.equals(
                        Constants.SUCCESS_CODE
                    )*/
                ) {
                    data.value = Resource(
                        Status.SUCCESS,
                        response.body(),
                        response.body()?.msg,
                        response.headers()
                    )
                } else {
                    data.value = Resource(
                        Status.ERROR,
                        response.body(),
                        response.body()?.msg,
                        response.headers()
                    )
                }
            }

            override fun onFailure(call: Call<JsonObjectResponse<CheckInModel>>, t: Throwable) {
                data.value = Resource(Status.FAILURE, null, t.localizedMessage)
            }
        })

        return data
    }







    /**
     * API for CheckIn Info
     */
    fun checkInOutInfo(checkInOutInput:  CheckInOutInput): LiveData<Resource<JsonObjectResponse<CheckInOutModel>>> {

        val data = MutableLiveData<Resource<JsonObjectResponse<CheckInOutModel>>>()

        data.value = Resource(Status.LOADING, null, "")

        apiService.checkInInfo(checkInOutInput).enqueue(object : Callback<JsonObjectResponse<CheckInOutModel>> {
            override fun onResponse(
                call: Call<JsonObjectResponse<CheckInOutModel>>,
                response: Response<JsonObjectResponse<CheckInOutModel>>
            ) {
                if (response.body()?.status.equals(Constants.SUCCESS) /*&& response.body()?.code.equals(
                        Constants.SUCCESS_CODE
                    )*/
                ) {
                    data.value = Resource(
                        Status.SUCCESS,
                        response.body(),
                        response.body()?.msg,
                        response.headers()
                    )
                } else {
                    data.value = Resource(
                        Status.ERROR,
                        response.body(),
                        response.body()?.msg,
                        response.headers()
                    )
                }
            }

            override fun onFailure(call: Call<JsonObjectResponse<CheckInOutModel>>, t: Throwable) {
                data.value = Resource(Status.FAILURE, null, t.localizedMessage)
            }
        })

        return data
    }
}