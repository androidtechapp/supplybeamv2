package org.genuinemark.supplybeam.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.genuinemark.supplybeam.model.UserModel
import org.genuinemark.supplybeam.api.input.LoginInput
import org.genuinemark.supplybeam.api.input.OTPInput
import org.genuinemark.supplybeam.api.response.ApiService
import org.genuinemark.supplybeam.api.response.JsonObjectResponse
import org.genuinemark.supplybeam.api.response.Resource
import org.genuinemark.supplybeam.api.response.Status
import org.genuinemark.supplybeam.models.SendOtpModel
import org.genuinemark.supplybeam.utils.Constants

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class LoginRepository constructor(var apiService: ApiService) {

    fun loginUser(otpInput: OTPInput): LiveData<Resource<JsonObjectResponse<SendOtpModel>>> {

        val data = MutableLiveData<Resource<JsonObjectResponse<SendOtpModel>>>()

        data.value = Resource(Status.LOADING, null, "")

        apiService.loginUser(otpInput).enqueue(object : Callback<JsonObjectResponse<SendOtpModel>> {
            override fun onResponse(
                call: Call<JsonObjectResponse<SendOtpModel>>,
                response: Response<JsonObjectResponse<SendOtpModel>>
            ) {
                if (response.body()?.status.equals(Constants.SUCCESS) /*&& response.body()?.code.equals(
                        Constants.SUCCESS_CODE
                    )*/
                ) {
                    data.value = Resource(
                        Status.SUCCESS,
                        response.body(),
                        response.body()?.msg,
                        response.headers()
                    )
                } else {
                    data.value = Resource(
                        Status.ERROR,
                        response.body(),
                        response.body()?.msg,
                        response.headers()
                    )
                }
            }

            override fun onFailure(call: Call<JsonObjectResponse<SendOtpModel>>, t: Throwable) {
                data.value = Resource(Status.FAILURE, null, t.localizedMessage)
            }
        })

        return data
    }


    fun otpVerify(loginInput: LoginInput): LiveData<Resource<JsonObjectResponse<UserModel>>> {

        val data = MutableLiveData<Resource<JsonObjectResponse<UserModel>>>()

        data.value = Resource(Status.LOADING, null, "")

        apiService.otpVerifyUser(loginInput).enqueue(object : Callback<JsonObjectResponse<UserModel>> {
            override fun onResponse(
                call: Call<JsonObjectResponse<UserModel>>,
                response: Response<JsonObjectResponse<UserModel>>
            ) {
                if (response.body()?.status.equals(Constants.SUCCESS) /*&& response.body()?.code.equals(
                        Constants.SUCCESS_CODE
                    )*/
                ) {
                    data.value = Resource(
                        Status.SUCCESS,
                        response.body(),
                        response.body()?.msg,
                        response.headers()
                    )
                } else {
                    data.value = Resource(
                        Status.ERROR,
                        response.body(),
                        response.body()?.msg,
                        response.headers()
                    )
                }
            }

            override fun onFailure(call: Call<JsonObjectResponse<UserModel>>, t: Throwable) {
                data.value = Resource(Status.FAILURE, null, t.localizedMessage)
            }
        })

        return data
    }

}