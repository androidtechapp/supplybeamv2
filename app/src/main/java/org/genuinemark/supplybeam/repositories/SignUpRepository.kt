package org.genuinemark.supplybeam.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.genuinemark.supplybeam.api.input.SignUpInput
import org.genuinemark.supplybeam.api.response.ApiService
import org.genuinemark.supplybeam.api.response.JsonObjectResponse
import org.genuinemark.supplybeam.api.response.Resource
import org.genuinemark.supplybeam.api.response.Status
import org.genuinemark.supplybeam.base.BaseResponse
import org.genuinemark.supplybeam.model.UserModel
import org.genuinemark.supplybeam.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Singleton


@Singleton
class SignUpRepository constructor(var apiService: ApiService) {

    fun getIsUsernameAvailable(username: String): LiveData<Resource<JsonObjectResponse<BaseResponse>>> {

        val data = MutableLiveData<Resource<JsonObjectResponse<BaseResponse>>>()

        apiService.getIsUsernameAvailable(hashMapOf("username" to username))
            .enqueue(object : Callback<JsonObjectResponse<BaseResponse>> {
                override fun onResponse(
                    call: Call<JsonObjectResponse<BaseResponse>>,
                    response: Response<JsonObjectResponse<BaseResponse>>
                ) {
                    if (response.body()?.status.equals(Constants.SUCCESS) && response.body()?.code.equals(
                            Constants.SUCCESS_CODE
                        )
                    ) {
                        data.value = Resource(
                            Status.SUCCESS,
                            response.body(),
                            response.body()?.msg,
                            response.headers()
                        )
                    } else {
                        data.value = Resource(
                            Status.ERROR,
                            response.body(),
                            response.body()?.msg,
                            response.headers()
                        )
                    }
                }

                override fun onFailure(call: Call<JsonObjectResponse<BaseResponse>>, t: Throwable) {
                    data.value = Resource(Status.FAILURE, null, t.localizedMessage)
                }
            })

        return data
    }


    fun registerUser(signUpInput: SignUpInput): LiveData<Resource<JsonObjectResponse<UserModel>>> {

        val data = MutableLiveData<Resource<JsonObjectResponse<UserModel>>>()

        data.value = Resource(Status.LOADING, null, "")

        apiService.registerUser(signUpInput)
            .enqueue(object : Callback<JsonObjectResponse<UserModel>> {
                override fun onResponse(
                    call: Call<JsonObjectResponse<UserModel>>,
                    response: Response<JsonObjectResponse<UserModel>>
                ) {
                    if (response.body()?.status.equals(Constants.SUCCESS) && response.body()?.code.equals(
                            Constants.SUCCESS_CODE
                        )
                    ) {
                        data.value = Resource(
                            Status.SUCCESS,
                            response.body(),
                            response.body()?.msg,
                            response.headers()
                        )
                    } else {
                        data.value = Resource(
                            Status.ERROR,
                            response.body(),
                            response.body()?.msg,
                            response.headers()
                        )
                    }
                }

                override fun onFailure(call: Call<JsonObjectResponse<UserModel>>, t: Throwable) {
                    data.value = Resource(Status.FAILURE, null, t.localizedMessage)
                }
            })

        return data
    }

}