package org.genuinemark.supplybeam.model

import android.text.TextUtils
import org.genuinemark.supplybeam.base.BaseModel
import com.google.gson.annotations.SerializedName

data class UserModel(

    @field:SerializedName("id")
    var id: String? = null,

    @field:SerializedName("name")
    var name: String? = null,

    @field:SerializedName("type")
    var type: String? = null,

    @field:SerializedName("mobile_no")
    var mobile_no: String? = null,

    @field:SerializedName("otp")
    var otp: String? = null,

    @field:SerializedName("email_id")
    var email_id: String? = null,

    @field:SerializedName("employee_id")
    var employee_id: String? = null,

    @field:SerializedName("user_id")
    var user_id: String? = null,

    @field:SerializedName("created_on")
    var created_on: String? = null,

    @field:SerializedName("modified_on")
    var modified_on: String? = null



) : BaseModel() {

    /*data class Data(


    )*/
}