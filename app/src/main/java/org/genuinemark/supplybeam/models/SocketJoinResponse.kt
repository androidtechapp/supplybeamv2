package org.genuinemark.supplybeam.model

import com.google.gson.annotations.SerializedName

data class SocketJoinResponse(
    @field:SerializedName("success")
    var nextpage: Boolean? = false,

    @field:SerializedName("data")
    var data: ArrayList<ChatMessageData>? = arrayListOf()
)