package org.genuinemark.supplybeam.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.genuinemark.supplybeam.base.BaseModel
import org.genuinemark.supplybeam.utils.Constants
import com.google.gson.annotations.SerializedName

@Entity(tableName = "chatTable")
data class ChatMessageData(

    @field:SerializedName("_id")
    @PrimaryKey
    @NonNull
    var _id: String = "",

    @field:SerializedName("group_id")
    var group_id: String? = null,

    @field:SerializedName("conversation_id")
    var conversation_id: String? = null,

    @field:SerializedName("last_message_time")
    var last_message_time: Long? = null,

    @field:SerializedName("sender_name")
    var sender_name: String? = null,

    @field:SerializedName("sender_image")
    var sender_image: String? = null,

    @field:SerializedName("type")
    var type: String?,

    @field:SerializedName("message")
    var message: String?,

    @field:SerializedName("video_url")
    var video_url: String?,

    @field:SerializedName("file_url")
    var files: ArrayList<String>? = arrayListOf(),

    @field:SerializedName("video_thumbnail")
    val video_thumbnail: String?,

    @field:SerializedName("sender_id")
    var sender_id: String? = null,

    @field:SerializedName("receiver_id")
    var receiver_id: String? = null,

    @field:SerializedName("read_status")
    var status: Int? = 0,

    @field:SerializedName("status")
    var msg_status: String? = null
) : BaseModel() {
    fun getImage(): String? {
        var url = ""
        try {
            if (!files.isNullOrEmpty()) {
                url =
                    Constants.AWS_BASE_URL + files!!.get(
                        0
                    )
            } else {
                url =
                    Constants.AWS_BASE_URL + video_thumbnail
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return url
    }

    fun getVideoUrl(): String? {
        var url = ""
        try {
            url =Constants.AWS_BASE_URL + video_url
            /*if (!files.isNullOrEmpty()) {
                url =
                    Constants.AWS_BASE_URL + files!!.get(files!!.size - 1)
            } else {
                url =Constants.AWS_BASE_URL + video_thumbnail
            }*/
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return url
    }

    fun getOpponentImage(): String? {
        var url = Constants.AWS_BASE_URL + sender_image
        return url
    }

    fun isTextType(): Boolean {
        return Constants.CHAT_MSG_TYPE_TEXT.equals(type, true)
    }

    fun isVideoType(): Boolean {
        return Constants.CHAT_MSG_TYPE_VIDEO.equals(type, true)
    }
}