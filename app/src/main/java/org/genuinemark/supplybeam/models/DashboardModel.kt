package org.genuinemark.supplybeam.models

import com.google.gson.annotations.SerializedName
import org.genuinemark.supplybeam.base.BaseModel
import org.genuinemark.supplybeam.model.UserModel

data class DashboardModel (

    @field:SerializedName("bannerImage")
    var bannerImage: String? = null,

    @field:SerializedName("totalChckOut")
    var totalChckOut: String? = null,

    @field:SerializedName("totalInHand")
    var totalInHand: String? = null,

    @field:SerializedName("totalInTransit")
    var totalInTransit: String? = null,

    @field:SerializedName("permission")
    var permission: Permission? = null

) : BaseModel(){



    data class Permission(

        @field:SerializedName("checkin")
        var checkin: Boolean? = null,

        @field:SerializedName("checkin + product tagging")
        var checkinProductTagging: Boolean? = null,

        @field:SerializedName("rdso certification")
        var rdsoCertification: Boolean? = null,

        @field:SerializedName("rejection checkout")
        var rejectionCheckout: Boolean? = null,

        @field:SerializedName("checkout")
        var checkout: Boolean? = null
    )
}