package org.genuinemark.supplybeam.models

import com.google.gson.annotations.SerializedName
import org.genuinemark.supplybeam.base.BaseModel
import org.genuinemark.supplybeam.model.UserModel

data class SendOtpModel(

    @field:SerializedName("message")
    var message: String? = null,

    @field:SerializedName("status")
    var status: String? = null


) : BaseModel() {
}