package org.genuinemark.supplybeam.models

import com.google.gson.annotations.SerializedName
import org.genuinemark.supplybeam.base.BaseModel

data class CheckInModel (

    @field:SerializedName("checkInData")
    var itemList: ArrayList<CheckInData>? = null

) : BaseModel() {


    data class CheckInData @JvmOverloads constructor(
        @field:SerializedName("id")
        var id: String? = null,

        @field:SerializedName("name")
        var name: String? = null,


        @field:SerializedName("isSelected")
        var isSelected: Boolean? = false

    ) : BaseModel()

}