package org.genuinemark.supplybeam.models

import com.google.gson.annotations.SerializedName
import org.genuinemark.supplybeam.base.BaseModel

data class CheckInOutModel (

    @field:SerializedName("checkInOutData")
    var itemList: ArrayList<CheckInOutData>? = null

) : BaseModel() {


    data class CheckInOutData @JvmOverloads constructor(
        @field:SerializedName("productName")
        var productName: String? = null,

        @field:SerializedName("productCode")
        var productCode: String? = null,


        @field:SerializedName("qrcode")
        var qrcode: String? = null,

        @field:SerializedName("qrcodeId")
        var qrcodeId: String? = null,

        @field:SerializedName("productId")
        var productId: String? = null


    ) : BaseModel()

}