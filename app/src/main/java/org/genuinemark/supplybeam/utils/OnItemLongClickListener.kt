package org.genuinemark.supplybeam.utils

import android.view.View


interface OnItemLongClickListener<T> {
    fun onLongItemClick(view: View, `object`: T, position: Int): Boolean
}