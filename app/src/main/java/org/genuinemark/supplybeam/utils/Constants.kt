package org.genuinemark.supplybeam.utils

import java.util.jar.Manifest


object Constants {

       const val BASE_URL = "http://genuinemark.org/mobileApi/admin/api/"  //Dev Environment
    // const val BASE_URL = "http://mymeetingdesk.com:80" //Staging Environment

    /*Amazon*/
    const val AWS_IDENTITY_POOL_ID = "s3_pool_id" // Staging
    const val AWS_REGION = "us-east-1"// Staging
    const val AWS_BUCKET_NAME = "s3_bucket_name"// Staging
    const val AWS_BASE_URL = "https://yogous-production.s3.amazonaws.com/"// Staging

    /*const val AWS_IDENTITY_POOL_ID = "s3_pool_id" // production
    const val AWS_REGION = "us-east-1"// production
    const val AWS_BUCKET_NAME = "s3_bucket_name"// production
    const val AWS_BASE_URL = "https://yogous-production.s3.amazonaws.com/"// production*/
    /*Stripe*/
    const val LINK_STRIPE_URL = "$BASE_URL/payment/link-stripe?token=" //Link Stripe
    const val ADD_MONEY_TO_WALLET_URL =
        "$BASE_URL/payment/init-checkout?token=" //Add Money to Wallet

    /*Stripe Success*/
    const val LINK_STRIPE_SUCCESS =
        "$BASE_URL/payment/redirected-back?state=" //Link Stripe Success
    const val ADD_MONEY_TO_WALLET_SUCCESS = "/checkout-success" //Add Money to Wallet Success

    /*Static Pages*/
    const val PRIVACY_URL = "https://www.yogous.com/privacy/"    /*"$BASE_URL/pages/Privacy"*/
    const val TERMS_URL = "https://www.yogous.com/terms-of-use/" /*"$BASE_URL/pages/Terms"*/
    const val BPA_URL =
        "https://www.yogous.com/business-participation-agreement/ " //Business Participation Agreement url
    const val CONTACT_URL = "$BASE_URL/pages/Contact"
    const val OS = "Android"

    const val SUCCESS = "1"
    const val ERROR = "error"

    const val SUCCESS_CODE = "200"
    const val BAD_REQUEST_CODE = "201"
    const val ERROR_CODE = "203"

    const val REQUEST_CSO = 1
    const val REQUEST_BUSINESS_CATEGORY = 2
    const val REQUEST_BUSINESS_TYPE = 3

    const val PROFILE_TYPE_PUBLIC = "public"
    const val PROFILE_TYPE_PRIVATE = "private"

    const val GENDER_MALE = "male"
    const val GENDER_FEMALE = "female"
    const val GENDER_OTHER = "other"
    const val GENDER_NO = "no"

    const val DEVICE_TOKEN = "1234567"
    const val SOCIAL_LOGIN_TYPE_GOOGLE = "google"
    const val GOOGLE_IMAGE_URL = "googleusercontent"

    const val FEED_LIKE = "like"
    const val FEED_UNLIKE = "unlike"

    const val FLAG_SPAM = "Spam"
    const val FLAG_INAPPROPRIATE = "Inappropriate"

    const val CBP_FEED_PENDING = "pending"
    const val CBP_FEED_ALL = "all"

    const val VIDEO_MAX_DURATION = 15

    const val FEED_TYPE_IMAGE = "image"
    const val FEED_TYPE_VIDEO = "video"

    const val RESPONSE_NO = "No"
    const val RESPONSE_YES = "Yes"

    //Media type
    const val MEDIA_TYPE_IMAGE = "image"
    const val MEDIA_TYPE_VIDEO = "video"


    const val AWS_FEED_FOLDER = "createPost/"

    val TAG: String = "tag"
    val USER_NAME: String = "user_name"
    const val HEADER = "Header"

    const val ERROR_440 = "440"  //Logged in from other device

    /*Activities Types*/
    const val ACTIVITY_TYPE_TAGGED = "tagged"
    const val ACTIVITY_TYPE_LIKE = "like"
    const val ACTIVITY_TYPE_LIKE_RECEIVED = "like_received"

    /*=========================================== Permission =============================================================*/
    val PERMISSION_REQUEST_QR_CAMERA = 24
    val PERMISSION_CAMERA = arrayOf(android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

    /*=========================================== Intent Keys =============================================================*/
    const val INTENT_KEY_IS_SOCIAL_SIGNUP = "is_social_signUp"
    const val INTENT_KEY_DATA = "data"
    const val INTENT_KEY_DATA_TWO = "data_two"
    const val INTENT_KEY_DATA_THREE = "data_three"
    const val INTENT_KEY_DATA_FOUR = "data_four"

    //=========================== Preference keys =======================================*/
    const val PREFERENCE_KEY_IS_LOGGED_IN = "is_user_logged_in"
    const val LAST_LOGGED_IN_USER = "last_logged_in_user"
    const val PREFERENCE_KEY_IS_PROFILE_COMPLETE = "is_profile_complete"
    const val PREFERENCE_KEY_IS_EMAIL_VERIFIED = "is_email_verified"
    const val PREFERENCE_KEY_IS_MOBILE_VERIFIED = "is_mobile_verified"
    const val PREFERENCE_KEY_IS_LOCATION_COMPLETE = "is_location_complete"
    const val PREFERENCE_KEY_IS_CSO_COMPLETE = "is_cso_complete"
    const val PREFERENCE_KEY_IS_PAYMENT_VERIFIED = "is_payment_verify"
    const val PREFERENCE_KEY_USER_DATA = "user_data"
    const val PREFERENCE_KEY_USER_TOKEN = "user_token"
    const val PREFERENCE_KEY_PROFILE_PIC = "social_image"
    const val PREFERENCE_KEY_USER_ID = "user_id"

    const val PREFERENCE_FIRST_TIME = "first_time"
    const val PREFERENCE_FCM_TOKEN = "fcm_token"

    //=============================== Date time constants =======================================*/
    val DATE_FORMAT_MM_DD_YYYY = "MMM dd, yyyy"
    val DATE_FORMAT_YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm"
    val DATE_FORMAT_MM_DD_YYYY_HH_MM = "MM-dd-yyyy hh:mm"
    val DATE_FORMAT_MM_DD_YYYY_HH_MM_A = "MM-dd-yyyy hh:mma"
    val DATE_FORMAT_HH_MM = "HH:MM"
    val DATE_FORMAT_HH_MM_A = "hh:mma"
    val DATE_FORMAT_hh_mm_aa = "hh:mm aa"
    val DATE_FORMAT_hh_mm_a = "hh:mm a"
    val DATE_FORMAT_YYYY_MM_DD_HH_mm_ss_SSS = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    val DATE_FORMAT_DEFAULT = DATE_FORMAT_MM_DD_YYYY
    val DATE_FORMAT_DD_MM_YYYY = "dd-MM-yyyy"
    val DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd"
    val DATE_FORMAT_DD_MMM_YYYY = "dd MMM yyyy"
    val DATE_FORMAT_YYYY_MMM_DD = "yyyy MMM dd"
    val DATE_FORMAT_E = "EEEE"

    const val ZIP: String = "1111"

    const val CONVERSATION_TYPE_CHAT: String = "chat"
    const val CONVERSATION_TYPE_GROUP: String = "group"

    const val MAX_VIDEO_DURATION: Int = 25// in seconds

    const val CHAT_MSG_TYPE_TEXT: String = "text"
    const val CHAT_MSG_TYPE_IMAGE: String = "image"
    const val CHAT_MSG_TYPE_VIDEO: String = "video"

    const val S3_CHAT_IMAGES: String = "chat_images"
    const val S3_CHAT_VIDEOS: String = "chat_videos"


    const val TARGET_ID: String = "target_id"
    const val TARGET_NAME: String = "target_name"
    const val CONVERSATION_TYPE: String = "conversation_type"

    const val DATE_FORMAT_HH_MM_DD_MMM_YYYY = "hh:mm a dd MMM yy"

    const val GROUP_MEMBERS_LIST = "group_members_list"
    const val VIDEO_URL = "video_url"
    const val MIN_GOOD_RATING = 2
    const val GROUP_ID = "group_id"
    val NAVIGATE_FROM = "navigate_from"
    val NOTIFICATION_TYPE = "notification_type"
    val NOTIFICATION_DATA = "notification_data"
    val NOTIFICATION_TITLE = "title"
    const val REFERRER = "referrer"
    const val USER_WELCOMED = "user_welcomed"
}