package org.genuinemark.supplybeam.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.model.CannedAccessControlList;

import java.io.File;

/**
 * Created by Phoosa Ram
 */

public class UploadFileResource {

    OnFileUploadListener onFileUploadListener;
    private Context mContext;

    public UploadFileResource(Context mContext) {
        this.mContext = mContext;
    }


    public void uploadToAmazon(File file, String tag, String fileName, TransferUtility transferUtility, OnFileUploadListener onFileUploadListener) {
        this.onFileUploadListener = onFileUploadListener;
        TransferObserver observer = transferUtility.upload(Constants.AWS_BUCKET_NAME, fileName, file, CannedAccessControlList.PublicReadWrite);
        observer.setTransferListener(new UploadImageListener(tag, fileName));
    }


    public interface OnFileUploadListener {
        void onSuccess(String tag, String fileName);

        void onProgress(int progress);

        void onError(String e);
    }

    private class UploadImageListener implements TransferListener {
        String fileName;
        String tag;

        public UploadImageListener(String tag, String fileName) {
            this.tag = tag;
            this.fileName = fileName;
        }

        @Override
        public void onError(int id, Exception e) {
            Log.e("Error", +id + " " + e);
            if (onFileUploadListener != null) {
                onFileUploadListener.onError("Failed to upload images, please try again.");
            }
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.e("progress", +bytesTotal + " " + bytesCurrent);
            if (onFileUploadListener != null) {
                onFileUploadListener.onProgress((int) (bytesCurrent / bytesTotal) * 100);
            }
        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.e("completed", " " + newState);
            if (newState.equals(TransferState.COMPLETED)) {
                Log.e("completed", " " + newState);
                if (onFileUploadListener != null) {
                    onFileUploadListener.onSuccess(tag, fileName);
                }
            } else if (newState.equals(TransferState.CANCELED) || newState.equals(TransferState.FAILED) || newState.equals(TransferState.PENDING_NETWORK_DISCONNECT)) {
                if (onFileUploadListener != null) {
                    onFileUploadListener.onError("Failed to upload images, please try again");
                }
                Toast.makeText(mContext, "Failed to upload images, please try again", Toast.LENGTH_LONG).show();
            }
        }
    }
}
