package org.genuinemark.supplybeam.utils

import android.content.Context
import androidx.preference.PreferenceManager



object SharedPreferenceUtils {

    @JvmStatic
    fun isMyUserId(context: Context, id: String?): Boolean? {
        if (id == null) {
            return false
        }
        return PreferenceManager.getDefaultSharedPreferences(context).getString(
            Constants.PREFERENCE_KEY_USER_ID,
            ""
        ) == id
    }

    @JvmStatic
    fun saveFCMToken(context: Context, fcmToken: String?) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
            .putString(Constants.PREFERENCE_FCM_TOKEN, fcmToken).apply()
    }
}