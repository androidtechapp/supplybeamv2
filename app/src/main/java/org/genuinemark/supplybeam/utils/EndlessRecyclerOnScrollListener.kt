package org.genuinemark.supplybeam.utils

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/*This class is used for scrolling recycler view to load more*/
abstract class EndlessRecyclerOnScrollListener : RecyclerView.OnScrollListener {

    private var previousTotal = 0 // The total number of items in the dataset after the last load
    private var loading = true // True if we are still waiting for the last set of data to load.
    private var visibleThreshold = 2 // The minimum amount of items to have below your current scroll position before loading more.
    internal var firstVisibleItem: Int = 0
    internal var visibleItemCount: Int = 0
    internal var totalItemCount: Int = 0

    private var mLinearLayoutManager: LinearLayoutManager? = null

    constructor(linearLayoutManager: LinearLayoutManager) {
        this.mLinearLayoutManager = linearLayoutManager
    }

    constructor(linearLayoutManager: LinearLayoutManager, visibleThreshold: Int) {
        this.mLinearLayoutManager = linearLayoutManager
        this.visibleThreshold = visibleThreshold
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        visibleItemCount = recyclerView.getChildCount()
        totalItemCount = mLinearLayoutManager!!.getItemCount()
        firstVisibleItem = mLinearLayoutManager!!.findFirstVisibleItemPosition()

        if (loading) {
            if (totalItemCount != previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }
        if (!loading && totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold) {
            //            if (!loading && firstVisibleItem > 0 && (totalItemCount - visibleItemCount)
            //                    <= (firstVisibleItem + visibleThreshold)) {
            // End has been reached

            onLoadMore()

            loading = true
        }
    }

    abstract fun onLoadMore()

    companion object {
        var TAG = EndlessRecyclerOnScrollListener::class.java.simpleName
    }

}