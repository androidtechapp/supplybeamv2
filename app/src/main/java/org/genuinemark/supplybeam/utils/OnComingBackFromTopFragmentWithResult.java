package org.genuinemark.supplybeam.utils;

public interface OnComingBackFromTopFragmentWithResult<T> {
    void onComingBack(T data);
}
