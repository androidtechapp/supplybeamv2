package org.genuinemark.supplybeam.utils

import android.app.Activity
import android.content.*
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.MediaMetadataRetriever
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Base64
import android.util.Log
import android.util.TypedValue
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.preference.PreferenceManager
import org.genuinemark.supplybeam.R
import org.genuinemark.supplybeam.base.BaseActivity
import org.genuinemark.supplybeam.utils.Constants.DATE_FORMAT_YYYY_MM_DD
import org.genuinemark.supplybeam.utils.Constants.DATE_FORMAT_hh_mm_a
import org.genuinemark.supplybeam.utils.Constants.PREFERENCE_KEY_USER_ID
import java.io.*
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


object SystemUtility {

    /**
     * hide the virtual keyboard from the current screen
     */
    fun hideVirtualKeyboard(_activity: Activity) {

        val view = _activity.window.currentFocus
        val imm = _activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (view != null)
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        else
        /* imm.hideSoftInputFromWindow((null == _activity.getCurrentFocus()) ? null : _activity.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);*/
            _activity.window.setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN
            )

    }

    fun printKeyHash(context: Context) {
        // Add code to print out the key hash
        try {
            val info = context.packageManager.getPackageInfo("com.oyraa", PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {
        } catch (e: NoSuchAlgorithmException) {
        }

    }

    @JvmStatic
    fun getTempMediaDirectory(context: Context): String? {
        val state = Environment.getExternalStorageState()
        var dir: File? = null
        if (Environment.MEDIA_MOUNTED == state) {
            dir = context.externalCacheDir
        } else {
            dir = context.cacheDir
        }

        if (dir != null && !dir.exists()) {
            dir.mkdirs()
        }
        return if (dir!!.exists() && dir.isDirectory) {
            dir.absolutePath
        } else null
    }

    fun validateFilePath(path: String): Boolean {
        return StringUtility.validateString(path) && validateFile(File(path))
    }

    fun validateFile(file: File): Boolean {
        return file.exists()
    }

    fun getId(context: Context): String?{
        return PreferenceManager.getDefaultSharedPreferences(context)
            .getString(PREFERENCE_KEY_USER_ID, "")
    }

    @JvmStatic
    fun getRealPathFromURI(context: Context, contentUri: Uri): String? {
        if (validateFilePath(contentUri.path!!)) {
            return contentUri.path
        }
        var cursor: Cursor? = null
        try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = context.contentResolver.query(contentUri, proj, null, null, null)
            if (cursor == null)
                return null
            val column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            return cursor.getString(column_index)
        } finally {
            cursor?.close()
        }
    }

    fun checkVideoDuration(context: Context, path: String): Long {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(context, Uri.parse(path))
        val time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
        val timeInMillisec = java.lang.Long.parseLong(time)
        return TimeUnit.MILLISECONDS.toSeconds(timeInMillisec)
    }

    fun createVideoThumbnail(filePath: String?, kind: Int): Bitmap? {
        var bitmap: Bitmap? = null
        val retriever = MediaMetadataRetriever()
        try {
            retriever.setDataSource(filePath)
            bitmap = retriever.getFrameAtTime(-1)
        } catch (ex: IllegalArgumentException) {
            // Assume this is a corrupt video file
        } catch (ex: RuntimeException) {
            // Assume this is a corrupt video file.
        } finally {
            try {
                retriever.release()
            } catch (ex: RuntimeException) {
                // Ignore failures while cleaning up.
            }

        }

        if (bitmap == null) return null

        /*if (kind == MediaStore.Images.Thumbnails.MINI_KIND) {
            // Scale down the bitmap if it's too large.
            val width = bitmap.width
            val height = bitmap.height
            val max = Math.max(width, height)
            if (max > 512) {
                val scale = 512f / max
                val w = Math.round(scale * width)
                val h = Math.round(scale * height)
                bitmap = Bitmap.createScaledBitmap(bitmap, w, h, true)
            }
        }*/

        if (bitmap.width >= bitmap.height){

            bitmap = Bitmap.createBitmap(
                bitmap,
                bitmap.width /2 - bitmap.height /2,
                0,
                bitmap.height,
                bitmap.height
            );

        }else{
            bitmap = Bitmap.createBitmap(
                bitmap,
                0,
                bitmap.height /2 - bitmap.width /2,
                bitmap.width,
                bitmap.width
            );
        }
        return bitmap
    }

    fun isNewGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.contentprovider" == uri.authority
    }

    fun getFileName(context: Context, uri: Uri): String {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                cursor!!.close()
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun isConnectedToInternet(context: Context): Boolean {
        val connectivity = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivity != null) {
            val info = connectivity.allNetworkInfo
            if (info != null)
                for (i in info.indices) {
                    if (info[i].state == NetworkInfo.State.CONNECTED) {
                        return true
                    }
                }
        }
        return false
    }

    fun dipToPx(c: Context, dipValue: Float): Int {
        val metrics = c.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics).toInt()

    }

    @JvmStatic
    fun capitalize(line: String?): String {
        return if (line != null && line.length > 0) {
            Character.toUpperCase(line[0]) + line.substring(1)
        } else ""
    }

    @JvmStatic
    fun isLoggedUser(userId: String?,context: Context): Boolean {
        if (userId != null) {
            if ((context as BaseActivity).getStringDataFromPreferences(PREFERENCE_KEY_USER_ID).equals(userId,true)) {
                return true
            }
            return false
        }
        return false
    }

    fun getLanguageCode(context: Context): String {
        return Locale.getDefault().language
    }

    /*fun buttonClickAnim(activity: Activity, view: View) {
        val animation = AnimationUtils.loadAnimation(activity.applicationContext, R.anim.button_anim)
        view.startAnimation(animation)
    }*/

    fun getFormattedTimeFromUnixTime(unixTime: Int): String {
        val c = Calendar.getInstance()
        c.timeInMillis = unixTime * 1000L
        val d = c.time
        return SimpleDateFormat(DATE_FORMAT_hh_mm_a, Locale.ENGLISH).format(d)
    }

    fun getFormattedDateFromUnixTime(unixTime: Int): String {
        val c = Calendar.getInstance()
        c.timeInMillis = unixTime * 1000L
        val d = c.time
        return SimpleDateFormat(DATE_FORMAT_YYYY_MM_DD, Locale.ENGLISH).format(d)
    }

    fun isDateToday(date: String): Boolean {
        val date1 = Date()
        return SimpleDateFormat(DATE_FORMAT_YYYY_MM_DD, Locale.ENGLISH).format(date1).toString() == date
    }

    fun getTodayDate(): String {
        val date1 = Date()
        return SimpleDateFormat(DATE_FORMAT_YYYY_MM_DD, Locale.ENGLISH).format(date1).toString()
    }

    fun getVideoDuration(context: Context, videoUri: Uri): Long {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(context, videoUri)
        val duration = java.lang.Long.parseLong(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION))
        retriever.release()
        return duration / 1000
    }

    fun getMediaDirectory(context: Context, type: String): String? {
        val state = Environment.getExternalStorageState()
        var dir: File? = null
        if (Environment.MEDIA_MOUNTED == state) {
            dir =
                File(Environment.getExternalStorageDirectory().toString() + "/" + context.getString(R.string.app_name) + " " + type + "s")
        } else {
            dir = File(context.cacheDir.toString() + "/" + context.getString(R.string.app_name) + " " + type + "s")
        }
        if (dir != null && !dir.exists()) {
            dir.mkdirs()
        }
        return if (dir.exists() && dir.isDirectory) {
            dir.absolutePath
        } else null
    }

    fun redirectToPlayStore(context: Context) {
        val uri = Uri.parse("market://details?id=" + context.packageName)
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(
            Intent.FLAG_ACTIVITY_NO_HISTORY or
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK
        )
        try {
            context.startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.packageName)
                )
            )
        }

    }

    fun getSecondsFromDurationString(value: String): Int {

        val parts = value.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        // Wrong format, no value for you.
        if (parts.size < 2 || parts.size > 3)
            return 0

        var seconds = 0
        var minutes = 0
        var hours = 0

        if (parts.size == 2) {
            seconds = Integer.parseInt(parts[1])
            minutes = Integer.parseInt(parts[0])
        } else if (parts.size == 3) {
            seconds = Integer.parseInt(parts[2])
            minutes = Integer.parseInt(parts[1])
            hours = Integer.parseInt(parts[0])
        }

        return seconds + minutes * 60 + hours * 3600
    }

   /* fun loadJSONFromAssets(context: Context): ArrayList<CountryModel> {
        var json: String? = null
        try {
            val inputStream = context.assets.open("country.json")
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()

            json = String(buffer, Charsets.UTF_8)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        val data =
            Gson().fromJson<List<CountryModel>>(json, object : TypeToken<List<CountryModel>>() {}.type)

        return data as ArrayList<CountryModel>
    }*/

    /**
     * Storing image to device gallery
     * @param cr
     * @param source
     * @param title
     * @param description
     * @return
     */
    fun insertImage(
        cr: ContentResolver,
        source: Bitmap?,
        title: String,
        description: String
    ): Uri? {

        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, title)
        values.put(MediaStore.Images.Media.DISPLAY_NAME, title)
        values.put(MediaStore.Images.Media.DESCRIPTION, description)
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
        // Add the date meta data to ensure the image is added at the front of the gallery
        values.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis())
        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())

        var url: Uri? = null
        var stringUrl: String? = null    /* value to be returned */

        try {
            url = cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)

            if (source != null) {
                val imageOut = cr.openOutputStream(url!!)
                try {
                    source.compress(Bitmap.CompressFormat.JPEG, 50, imageOut)
                } finally {
                    imageOut!!.close()
                }

                val id = ContentUris.parseId(url)
                // Wait until MINI_KIND thumbnail is generated.
                val miniThumb =
                    MediaStore.Images.Thumbnails.getThumbnail(cr, id, MediaStore.Images.Thumbnails.MINI_KIND, null)
                // This is for backward compatibility.
                storeThumbnail(cr, miniThumb, id, 50f, 50f, MediaStore.Images.Thumbnails.MICRO_KIND)
            } else {
                cr.delete(url!!, null, null)
                url = null
            }
        } catch (e: Exception) {
            if (url != null) {
                cr.delete(url, null, null)
                url = null
            }
        }

        if (url != null) {
            stringUrl = url.toString()
        }

        return url
    }

    /**
     * A copy of the Android internals StoreThumbnail method, it used with the insertImage to
     * populate the android.provider.MediaStore.Images.Media#insertImage with all the correct
     * meta data. The StoreThumbnail method is private so it must be duplicated here.
     *
     * @see android.provider.MediaStore.Images.Media
     */
    private fun storeThumbnail(
        cr: ContentResolver,
        source: Bitmap,
        id: Long,
        width: Float,
        height: Float,
        kind: Int
    ): Bitmap? {

        // create the matrix to scale it
        val matrix = Matrix()

        val scaleX = width / source.width
        val scaleY = height / source.height

        matrix.setScale(scaleX, scaleY)

        val thumb = Bitmap.createBitmap(
            source, 0, 0,
            source.width,
            source.height, matrix,
            true
        )

        val values = ContentValues(4)
        values.put(MediaStore.Images.Thumbnails.KIND, kind)
        values.put(MediaStore.Images.Thumbnails.IMAGE_ID, id.toInt())
        values.put(MediaStore.Images.Thumbnails.HEIGHT, thumb.height)
        values.put(MediaStore.Images.Thumbnails.WIDTH, thumb.width)

        val url = cr.insert(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, values)

        try {
            val thumbOut = cr.openOutputStream(url!!)
            thumb.compress(Bitmap.CompressFormat.JPEG, 100, thumbOut)
            thumbOut!!.close()
            return thumb
        } catch (ex: FileNotFoundException) {
            return null
        } catch (ex: IOException) {
            return null
        }

    }


    @JvmStatic
    fun getImageUri(context: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path =
            MediaStore.Images.Media.insertImage(context.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }
}