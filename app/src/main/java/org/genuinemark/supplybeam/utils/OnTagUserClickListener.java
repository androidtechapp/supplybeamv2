package org.genuinemark.supplybeam.utils;


public interface OnTagUserClickListener<T> {
    void onTagUserClicked(String userName, T commentsItem);
}
