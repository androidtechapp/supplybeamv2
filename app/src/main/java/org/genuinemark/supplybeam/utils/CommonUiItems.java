package org.genuinemark.supplybeam.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;


public class CommonUiItems {
    public static AlertDialog myDialog(Context context, String title, String msg, String pB, String nB){
        if(((Activity) context).isFinishing()) {
            return null;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
                .setMessage(msg);
        if(pB!=null) {
            builder.setPositiveButton(pB, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        }
        if(nB!=null){
            builder.setNegativeButton(nB, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        }
        AlertDialog dialog = builder.create();
        return dialog;
    }

    public static AlertDialog myDialog(Context context, String title, String msg, String pB, String nB, DialogInterface.OnClickListener pListener, DialogInterface.OnClickListener nListener){
        if(((Activity) context).isFinishing()) {
            return null;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
                .setMessage(msg);
        if(pB!=null) {
            builder.setPositiveButton(pB,pListener);
        }
        if(nB!=null){
            builder.setNegativeButton(nB,nListener);
        }
        AlertDialog dialog = builder.create();
        dialog.show();
        return dialog;
    }



    public static void hideKeyboard(Activity activity){
        if(activity.isFinishing()) {
            return;
        }
        View view1 = activity.getCurrentFocus();
        if (view1 != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view1.getWindowToken(), 0);
        }
    }

    public static Dialog getCustomDialog(Context context, int layout){
        Dialog dialog;
        dialog = new Dialog(context,android.R.style.Theme_Material_Light_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(layout);
        dialog.setCancelable(true);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
        return dialog;
    }
}
