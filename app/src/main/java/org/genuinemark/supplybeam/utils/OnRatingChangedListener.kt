package org.genuinemark.supplybeam.utils

import android.view.View



interface OnRatingChangedListener<T> {
    fun onRatingChanged(view: View, rating: Float, fromUser: Boolean, `object`: T, position: Int)
}