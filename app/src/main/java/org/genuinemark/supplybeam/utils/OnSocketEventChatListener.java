package org.genuinemark.supplybeam.utils;

public interface OnSocketEventChatListener {
    void onNewMessage(String response);
}
