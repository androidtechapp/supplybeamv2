package org.genuinemark.supplybeam.utils;


import java.security.SecureRandom;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

public class RandomString {

    public String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public String lower = upper.toLowerCase(Locale.ROOT);
    public String digits = "0123456789";
    public String alphanum = upper + lower + digits;
    private Random random;
    private char[] symbols;
    private char[] buf;

    public RandomString(int length, Random random, String symbols) {

        if (length < 1) throw new IllegalArgumentException();
        if(symbols!=null)
        if (symbols.length() < 2) throw new IllegalArgumentException();
        this.random = Objects.requireNonNull(random);
        if (symbols == null) {
            this.symbols = new String(upper + lower + digits).toCharArray();
        } else {
            this.symbols = symbols.toCharArray();
        }
        this.buf = new char[length];
    }

    /**
     * Create an alphanumeric string generator.
     */
    public RandomString(int length, Random random) {
        this(length, random, null);
    }

    /**
     * Create an alphanumeric strings from a secure generator.
     */
    public RandomString(int length) {
        this(length, new SecureRandom());
    }

    /**
     * Create session identifiers.
     */
    public RandomString() {
        this(21);
    }

    /**
     * Generate a random string.
     */
    public String nextString() {
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        return new String(buf);
    }

}
