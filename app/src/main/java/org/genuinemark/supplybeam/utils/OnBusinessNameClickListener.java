package org.genuinemark.supplybeam.utils;

public interface OnBusinessNameClickListener {
    void onBusinessNameClick(String businessId);
}
