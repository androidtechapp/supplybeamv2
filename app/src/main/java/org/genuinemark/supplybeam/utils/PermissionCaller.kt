package org.genuinemark.supplybeam.utils

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat


class PermissionCaller private constructor(internal var context: Activity) {

    fun isCameraPermission(fineReq: Int): Boolean? {
        val isFine = ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
        if (isFine) {
            return true
        } else {
            reqCameraPermission(fineReq)
            return false
        }
    }

    fun isAccessLocation(fineReq: Int): Boolean? {
        val isFine = ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
        val isCoarse = ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
        if (isFine && isCoarse) {
            return true
        } else {
            reqAccessLocation(fineReq)
            return false
        }
    }

    fun isAccessReadWrite(fineReq: Int): Boolean? {
        val isRead = ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
        val isWrite = ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED
        if (isRead && isWrite) {
            return true
        }
        if (!isRead && !isWrite) {
            reqAccessReadWrite(fineReq)
            return false
        } else {
            if (!isRead)
                reqAccessRead(fineReq)
            if (!isWrite)
                reqAccessWrite(fineReq)
        }
        return false
    }

    fun isAccessAudioRecord(fineReq: Int): Boolean? {
        val isAudio = ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED
        val isAudioSet = ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.MODIFY_AUDIO_SETTINGS
        ) == PackageManager.PERMISSION_GRANTED

        if (isAudio && isAudioSet) {
            return true
        }

        reqAccessAudio(fineReq)
        return false
    }

    fun isGetAccount(fineReq: Int): Boolean? {
        val isFine = ActivityCompat.checkSelfPermission(
            context,
            Manifest.permission.GET_ACCOUNTS
        ) == PackageManager.PERMISSION_GRANTED

        if (isFine) {
            return true
        } else {
            reqGetAccount(fineReq)
            return false
        }
    }

    fun isListOfPermission(fineReq: Array<String>, reqAll: Int): Boolean? {
        var isOk = true
        val perNeed = StringBuilder()
        for (per in fineReq) {
            if (!(ActivityCompat.checkSelfPermission(context, per) == PackageManager.PERMISSION_GRANTED)) {
                if (isOk)
                    isOk = false
                perNeed.append(per)
                perNeed.append(",")
            }
        }

        if (isOk) {
            return true
        }

        val permissions = if (perNeed.length > 1) perNeed.substring(0, perNeed.length - 1).toString() else ""
        if (!permissions.isEmpty()) {
            val arrPer = permissions.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            reqAllPermissions(arrPer, reqAll)
        }

        return false
    }

    fun reqCameraPermission(fineReq: Int) {
        ActivityCompat.requestPermissions(
            context,
            arrayOf(Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION),
            fineReq
        )
    }

    fun reqAccessLocation(fineReq: Int) {
        ActivityCompat.requestPermissions(
            context,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
            fineReq
        )
    }

    fun reqAccessReadWrite(fineReq: Int) {
        ActivityCompat.requestPermissions(
            context,
            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
            fineReq
        )
    }

    fun reqAccessAudio(fineReq: Int) {
        ActivityCompat.requestPermissions(
            context,
            arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.MODIFY_AUDIO_SETTINGS),
            fineReq
        )
    }

    fun reqAccessRead(fineReq: Int) {
        ActivityCompat.requestPermissions(context, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), fineReq)
    }

    fun reqAccessWrite(fineReq: Int) {
        ActivityCompat.requestPermissions(context, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), fineReq)
    }


    fun reqGetAccount(fineReq: Int) {
        ActivityCompat.requestPermissions(context, arrayOf(Manifest.permission.GET_ACCOUNTS), fineReq)
    }

    fun reqAllPermissions(permissions: Array<String>, fineReq: Int) {
        ActivityCompat.requestPermissions(context, permissions, fineReq)
    }

    companion object {

        fun getInstance(context: Activity): PermissionCaller {
            return PermissionCaller(context)
        }
    }
}