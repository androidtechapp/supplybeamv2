package org.genuinemark.supplybeam.utils

interface InviteLinkCallback {
    fun onLinkGenerated(inviteLink:String?)
}