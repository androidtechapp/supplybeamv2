package org.genuinemark.supplybeam.utils;

public interface OnComingBackFromTopFragment {
    void onComingBack();
}
