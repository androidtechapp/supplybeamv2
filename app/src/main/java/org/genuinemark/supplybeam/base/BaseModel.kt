package org.genuinemark.supplybeam.base

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import org.genuinemark.supplybeam.BR
import java.io.Serializable

open class BaseModel : BaseObservable(), Serializable {

    private var selected = false
    private var userToken: String? = ""

    fun setUserToken(userToken: String?) {
        this.userToken = userToken
    }

    fun getUserToken(): String? {
        return userToken
    }

    @Bindable
    fun isSelected(): Boolean {
        return selected
    }

    fun setSelected(selected: Boolean) {
        this.selected = selected
        //notifyPropertyChanged(BR.selected)
    }

}