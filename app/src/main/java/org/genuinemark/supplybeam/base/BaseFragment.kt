package org.genuinemark.supplybeam.base

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.preference.PreferenceManager
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import okhttp3.Headers
import org.genuinemark.supplybeam.R
import org.genuinemark.supplybeam.SupplyBeam
import org.genuinemark.supplybeam.utils.Constants
import org.genuinemark.supplybeam.utils.PermissionCaller
import org.genuinemark.supplybeam.utils.StringUtility
import org.genuinemark.supplybeam.utils.SystemUtility
import java.io.File
import java.io.IOException


open class BaseFragment : Fragment(), View.OnClickListener {


    val REQUEST_CAPTURE = 1001
    val REQUEST_GALLERY = 1002
    var captureMediaFile: Uri? = null
    lateinit var dialog: AlertDialog
    lateinit var dialogBuilder: AlertDialog.Builder
    private var mContext: Context? = null

    val REQUEST_COMMENT_DIALOG = 1004
    val REQUEST_FLAG_DIALOG = 1005
    val REQUEST_REMOVE_TAG_DIALOG = 1007
    val REQUEST_CODE_EDIT_PROFILE = 1006
    val REQUEST_BUSINESS_LISTING_DIALOG = 1008
    val REQUEST_ADD_AMOUNT_SUCCESS = 1009
    val REQUEST_LINK_STRIPE_SUCCESS = 1010

    override fun onAttach(context: Context) {
        super.onAttach(context)

        this.mContext = context
        dialogBuilder = AlertDialog.Builder(context)
        dialogBuilder.setCancelable(false)
        dialogBuilder.setView(R.layout.layout_progress_dialog)
        dialog = dialogBuilder.create()
    }

    /*To show progress bar*/
    fun enableLoadingBar(enable: Boolean) {
        if (enable) {
            dialog.show()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.argb(0, 200, 200, 200)))
        } else {
            dialog.dismiss()
        }
    }


    fun tag(): String {
        return javaClass.simpleName
    }

    fun log(message: String) {
        Log.d(tag(), message)
    }

    fun toast(message: String?) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    fun getAlertDialogBuilder(
        title: String?,
        message: String,
        cancellable: Boolean
    ): AlertDialog.Builder {
        return AlertDialog.Builder(this.activity!!)
            .setTitle(title)
            .setMessage(message)
            .setCancelable(cancellable)

    }

    fun onError(reason: String) {
        if (activity != null && isAdded)
            onError(reason, false)
    }

    fun onError(reason: String, finishOnOk: Boolean) {
        if (StringUtility.validateString(reason)) {
            getAlertDialogBuilder(null, reason, false).setPositiveButton(getString(R.string.ok),
                if (finishOnOk)
                    DialogInterface.OnClickListener { dialogInterface, i -> this.activity!!.finish() }
                else
                    null).show()
        } else {
            getAlertDialogBuilder(null, getString(R.string.default_error), false)
                .setPositiveButton(getString(R.string.ok), if (finishOnOk)
                    DialogInterface.OnClickListener { dialogInterface, i -> this.activity!!.finish() }
                else
                    null).show()
        }
    }

    fun onInfo(message: String) {
        onInfo(message, false)
    }

    fun onInfo(message: String, finishOnOk: Boolean) {
        getAlertDialogBuilder(null, message, false).setPositiveButton(getString(R.string.ok),
            if (finishOnOk)
                DialogInterface.OnClickListener { dialogInterface, i -> this.activity!!.finish() }
            else
                null).show()
    }

    fun onNoInternet() {
        getAlertDialogBuilder(
            getString(R.string.no_internet),
            getString(R.string.no_network_message),
            true
        ).setPositiveButton(
            getString(R.string.ok),
            null
        ).show()
    }

    fun pickImageFromGallery() {
        if (!PermissionCaller.getInstance(this.activity!!).isListOfPermission(
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                ), REQUEST_GALLERY
            )!!
        )
            return
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        intent.type = "image/*"
        startActivityForResult(
            Intent.createChooser(intent, getString(R.string.select_image)),
            REQUEST_GALLERY
        )
    }

    fun pickImageFromCamera() {
        if (!PermissionCaller.getInstance(this.activity!!).isListOfPermission(
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                ), REQUEST_CAPTURE
            )!!
        )
            return
        val intent = Intent()
        intent.action = MediaStore.ACTION_IMAGE_CAPTURE
        try {
            val tempFile =
                File.createTempFile(
                    "image",
                    ".png",
                    File(SystemUtility.getTempMediaDirectory(this.activity!!)!!)
                )
            captureMediaFile =
                FileProvider.getUriForFile(this.activity!!, "com.app.oyraa.provider", tempFile)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, captureMediaFile)

            val resInfoList =
                this.activity!!.getPackageManager()
                    .queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
            for (resolveInfo in resInfoList) {
                val packageName = resolveInfo.activityInfo.packageName
                this.activity!!.grantUriPermission(
                    packageName,
                    captureMediaFile,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
                )
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        startActivityForResult(intent, REQUEST_CAPTURE)
    }

    fun replaceFragment(@IdRes container: Int, fragment: Fragment) {
        replaceFragment(container, fragment, null)
    }

    fun replaceFragment(@IdRes container: Int, fragment: Fragment, arguments: Bundle?) {
        if (arguments != null) {
            fragment.setArguments(arguments)
        }
        childFragmentManager.beginTransaction().replace(container, fragment).addToBackStack(null)
            .commit()
    }

    fun addFragment(@IdRes container: Int, fragment: Fragment, arguments: Bundle?) {
        if (arguments != null) {
            fragment.arguments = arguments
        }
        childFragmentManager.beginTransaction().add(container, fragment).addToBackStack(null)
            .commit()
    }

    fun isListOfPermission(fineReq: Array<String>, reqAll: Int): Boolean? {
        var isOk = true
        val perNeed = StringBuilder()
        for (per in fineReq) {
            if (!(ActivityCompat.checkSelfPermission(this.activity!!, per) == PackageManager.PERMISSION_GRANTED)) {
                if (isOk)
                    isOk = false
                perNeed.append(per)
                perNeed.append(",")
            }
        }

        if (isOk) {
            return true
        }

        val permissions =
            if (perNeed.length > 1) perNeed.substring(0, perNeed.length - 1).toString() else ""
        if (!permissions.isEmpty()) {
            val arrPer =
                permissions.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            requestPermissions(arrPer, reqAll)
        }

        return false
    }

    /*============================    Shared Preferences   =====================================*/

    fun putStringDataInPreferences(key: String, value: String?) {
        PreferenceManager.getDefaultSharedPreferences(activity).edit().putString(key, value).apply()
    }

    fun getStringDataFromPreferences(key: String): String? {
        return PreferenceManager.getDefaultSharedPreferences(activity).getString(key, "")
    }

    fun putBooleanDataInPreferences(key: String, value: Boolean?) {
        PreferenceManager.getDefaultSharedPreferences(activity).edit().putBoolean(key, value!!)
            .apply()
    }

    fun getBooleanDataFromPreferences(key: String): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(activity).getBoolean(key, false)
    }

    fun clearPreferences() {
        PreferenceManager.getDefaultSharedPreferences(activity).edit().clear().apply()
    }

    override fun onClick(v: View?) {

    }

    fun isCameraPermission(): Boolean{
        if (Build.VERSION.SDK_INT >= 23){
            if(ActivityCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    Constants.PERMISSION_CAMERA,
                    Constants.PERMISSION_REQUEST_QR_CAMERA
                )
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }

    /*fun hideKeyBoard() {
        try {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(
                findViewById<View>(android.R.id.content).getWindowToken(),
                0
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }*/

    fun redirectToPlayStore() {
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setData(Uri.parse("market://details?id=" + activity?.getPackageName()))
            startActivity(intent)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    fun processHeaders(headers: Headers?) {
        try {
            val updateAvailable = headers!!.get("update_available")
            val forceUpdateRequired = headers.get("force_update")
            if ("1".equals(updateAvailable)) {
                if ("1".equals(forceUpdateRequired)) {
                    try {
                        getAlertDialogBuilder(
                            getString(R.string.update_available),
                            getString(R.string.new_version_available),
                            false
                        ).setPositiveButton(
                            getString(R.string.update),
                            DialogInterface.OnClickListener { dialogInterface, i -> redirectToPlayStore() }
                        ).show()
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                } else {
                    if (SupplyBeam.dialogShownForUpdate != 1) {
                        try {
                            getAlertDialogBuilder(
                                getString(R.string.update_available),
                                getString(R.string.new_version_available_optional),
                                true
                            ).setNegativeButton(
                                getString(R.string.no), null
                            ).setPositiveButton(
                                getString(R.string.yes),
                                DialogInterface.OnClickListener { dialogInterface, i -> redirectToPlayStore() }
                            ).show()
                            SupplyBeam.dialogShownForUpdate = 1
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


}