package org.genuinemark.supplybeam.base

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



open class BaseResponse {
    @SerializedName("type")
    @Expose
    var type: Boolean? = null
    @SerializedName("limit")
    @Expose
    var limit: Int? = null
    @SerializedName("page")
    @Expose
    var page: String? = null
    @SerializedName("next")
    @Expose
    var next: Boolean? = null

    @field:SerializedName("msg")
    val msg: String? = null

    @field:SerializedName("code")
    val code: String? = null

    @field:SerializedName("status")
    val status: String? = null

    @field:SerializedName("nextpage")
    val nextpage: String? = null
}