package org.genuinemark.supplybeam.base

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.TransferNetworkLossHandler
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.CannedAccessControlList
import org.genuinemark.supplybeam.R
import org.genuinemark.supplybeam.api.response.JsonObjectResponse
import org.genuinemark.supplybeam.api.response.Resource
import org.genuinemark.supplybeam.utils.Constants
import org.genuinemark.supplybeam.utils.Constants.VIDEO_MAX_DURATION
import org.genuinemark.supplybeam.utils.PermissionCaller
import org.genuinemark.supplybeam.utils.SystemUtility
import com.google.gson.Gson
import com.theartofdev.edmodo.cropper.BuildConfig
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import okhttp3.Headers
import org.genuinemark.supplybeam.SupplyBeam
import org.genuinemark.supplybeam.model.UserModel
import org.genuinemark.supplybeam.ui.activities.DashboardActivity
import org.genuinemark.supplybeam.utils.Constants.PERMISSION_REQUEST_QR_CAMERA
import org.genuinemark.supplybeam.utils.Constants.PREFERENCE_KEY_USER_ID
import java.io.File
import java.io.IOException


open class BaseActivity : AppCompatActivity(), View.OnClickListener  {



    val REQUEST_CAPTURE = 1001
    val REQUEST_GALLERY = 1002
    val REQUEST_VIDEO_GALLERY = 1003
    val REQUEST_VIDEO_CAPTURE = 1004
    val REQUEST_VIDEO_TRIM = 1005
    val AUTOCOMPLETE_REQUEST_CODE = 1007
    val REQUEST_CODE_REVIEW_POST = 1008
    lateinit var captureMediaFile: Uri
    lateinit var captureVideoFile: Uri
    lateinit var dialog: AlertDialog
    lateinit var dialogBuilder: AlertDialog.Builder
    lateinit var s3: AmazonS3
    lateinit var transferUtility: TransferUtility
    lateinit var uploadObserver: TransferObserver
    lateinit var tempVideoFile: File
    var currentFragment: Fragment? = null

    var tempFile: File? = null
    var hideKeyboardOnOutsideTouch: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setCancelable(false)
        dialogBuilder.setView(R.layout.layout_progress_dialog)
        dialog = dialogBuilder.create()

        initAmazon()
    }

    override fun onResume() {
        super.onResume()
        SupplyBeam.currentActivity = this;
        shouldHideKeyboardOnOutsideTouch(true)
        if (getBooleanDataFromPreferences(Constants.PREFERENCE_KEY_IS_LOGGED_IN)) {
            startService()
        }
    }

    override fun onPause() {
        super.onPause()
        SupplyBeam.currentActivity = null;
    }

    fun shouldHideKeyboardOnOutsideTouch(shouldHide: Boolean) {
        this.hideKeyboardOnOutsideTouch = shouldHide
    }

    /*override fun hideKeyboard() {
        runOnUiThread{
            mBaseViewModel.keyboardController.value = false
        }
    }

    override fun hideKeyboard(v: View) {
        runOnUiThread {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(v.windowToken, 0)
        }
    }

    override fun showKeyboard() {
        runOnUiThread {
            mBaseViewModel.keyboardController.value = true
        }
    }*/


    fun startService() {
        if (getBooleanDataFromPreferences(Constants.PREFERENCE_KEY_IS_LOGGED_IN)) {
            //WebSocketService.startService(this, this)
        }
    }

    open fun captureResult(uri: Uri?, requestCode: Int) {
        try {
            val mimeType = contentResolver.getType(uri!!)
            if (!TextUtils.isEmpty(mimeType) && mimeType!!.startsWith("image")) {
                CropImage.activity(uri).setFixAspectRatio(false).setOutputCompressQuality(50)
                    .start(this)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    private fun initAmazon() {

        s3 = AmazonS3Client(
            CognitoCachingCredentialsProvider(
                this,
                Constants.AWS_IDENTITY_POOL_ID,
                Regions.fromName(Constants.AWS_REGION)
            ),
            Region.getRegion(Constants.AWS_REGION)
        )

        transferUtility = TransferUtility.builder().context(this.applicationContext)
            .s3Client(s3)
            .build()

        TransferNetworkLossHandler.getInstance(this)
    }

    fun uploadMediaToAWS(mediaPath: String?, mediaName: String) {
        uploadObserver =
            transferUtility.upload(
                Constants.AWS_BUCKET_NAME,
                mediaName,
                File(mediaPath!!),
                CannedAccessControlList.PublicRead
            )

        enableLoadingBar(true)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        try {
            if (hideKeyboardOnOutsideTouch) {
                val view: View? = currentFocus
                if (view != null && (ev?.action == MotionEvent.ACTION_UP || ev?.action == MotionEvent.ACTION_MOVE) && view is EditText) {
                    val scrcoords = IntArray(2);
                    view.getLocationOnScreen(scrcoords);
                    val x = ev.rawX + view.getLeft() - scrcoords[0];
                    val y = ev.rawY + view.getTop() - scrcoords[1];
                    if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                            window.decorView.applicationWindowToken,
                            0
                        )
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace();
        }
        return super.dispatchTouchEvent(ev)
    }

    fun hideKeyBoard() {
        try {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(
                findViewById<View>(android.R.id.content).getWindowToken(),
                0
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun getCurrentFragment(@IdRes container: Int): Fragment? {
        return supportFragmentManager.findFragmentById(container);
    }

    internal fun toast(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    internal fun log(message: String) {
        Log.e(javaClass.simpleName, message)
    }

    fun replaceFragment(@IdRes container: Int, fragment: Fragment) {
        replaceFragment(container, fragment, null)
    }

    fun replaceFragment(@IdRes container: Int, fragment: Fragment, arguments: Bundle?) {
        if (arguments != null) {
            fragment.arguments = arguments
        }
        currentFragment = fragment
        supportFragmentManager.beginTransaction().replace(container, fragment).commit()
    }

    fun replaceFragment(
        @IdRes container: Int, fragment: Fragment,
        arguments: Bundle?,
        addBackStack: Boolean
    ) {
        if (arguments != null) {
            fragment.setArguments(arguments)
        }
        currentFragment = fragment
        supportFragmentManager.beginTransaction().replace(container, fragment)
            .commit()
    }

    fun addFragment(@IdRes container: Int, fragment: Fragment) {
        addFragment(container, fragment, null)
    }

    fun addFragment(@IdRes container: Int, fragment: Fragment, arguments: Bundle?) {
        if (arguments != null) {
            fragment.arguments = arguments
        }
        supportFragmentManager.beginTransaction().add(container, fragment).addToBackStack(null)
            .commit()
    }

    fun getAlertDialogBuilder(
        title: String?,
        message: String?,
        cancellable: Boolean
    ): AlertDialog.Builder {
        return AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setCancelable(cancellable)

    }

    fun onInfo(message: String) {
        onInfo(message, false)
    }

    fun onInfo(message: String?, finishOnOk: Boolean) {
        getAlertDialogBuilder(null, message, false).setPositiveButton(getString(R.string.ok),
            if (finishOnOk)
                DialogInterface.OnClickListener { dialogInterface, i -> finish() }
            else
                null).show()
    }

    fun onNoInternet() {
        getAlertDialogBuilder(
            getString(R.string.no_internet),
            getString(R.string.no_network_message),
            true
        ).setPositiveButton(
            getString(R.string.ok),
            null
        ).show()
    }

    /*To show progress bar*/
    fun enableLoadingBar(enable: Boolean) {
        if (enable && !isFinishing) {
            dialog.show()
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.argb(0, 200, 200, 200)))
        } else {
            dialog.dismiss()
        }
    }

    fun dialogImagePicker() {
        val imagePicker = AlertDialog.Builder(this)
        imagePicker.setItems(
            arrayOf(getString(R.string.pick_from_gallery), getString(R.string.take_from_camera))
        ) { dialog, which ->
            when (which) {
                0 -> pickImageFromGallery()
                1 -> pickImageFromCamera()
                else -> {
                }
            }
        }.setCancelable(true).setTitle(getString(R.string.select_image)).show()
    }

    fun pickImageFromGallery() {
        if (!PermissionCaller.getInstance(this).isListOfPermission(
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), REQUEST_GALLERY
            )!!
        )
            return
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        intent.type = "image/*"
        startActivityForResult(
            Intent.createChooser(intent, getString(R.string.select_image)),
            REQUEST_GALLERY
        )
    }

    fun pickImageFromCamera() {
        if (!PermissionCaller.getInstance(this).isListOfPermission(
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                ), REQUEST_CAPTURE
            )!!
        )
            return
        val intent = Intent()
        intent.action = MediaStore.ACTION_IMAGE_CAPTURE
        try {
            val tempFile = File.createTempFile(
                "image",
                ".png",
                File(SystemUtility.getTempMediaDirectory(this)!!)
            )
            captureMediaFile =
                FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", tempFile)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, captureMediaFile)

            val resInfoList =
                packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
            for (resolveInfo in resInfoList) {
                val packageName = resolveInfo.activityInfo.packageName
                grantUriPermission(
                    packageName,
                    captureMediaFile,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
                )
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        startActivityForResult(intent, REQUEST_CAPTURE)
    }

    fun startCropImage(sourceFileUri: Uri) {
        startCropImage(sourceFileUri, false)
    }

    fun startCropImage(sourceFileUri: Uri, isAspectRatio: Boolean) {
        if (isAspectRatio) {
            CropImage.activity(sourceFileUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .start(this)
        } else {
            CropImage.activity(sourceFileUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this)
        }

    }


    fun pickVideoFromCamera() {
        if (!PermissionCaller.getInstance(this).isListOfPermission(
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                ), REQUEST_VIDEO_CAPTURE
            )!!
        )
            return
        val takeVideoIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, VIDEO_MAX_DURATION)
        try {
            tempVideoFile = File.createTempFile(
                "video",
                ".mp4",
                File(SystemUtility.getTempMediaDirectory(this)!!)
            )
            captureVideoFile = FileProvider.getUriForFile(
                this,
                BuildConfig.APPLICATION_ID + ".provider",
                tempVideoFile
            )
            takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, captureVideoFile)

            val resInfoList = packageManager.queryIntentActivities(
                takeVideoIntent,
                PackageManager.MATCH_DEFAULT_ONLY
            )
            for (resolveInfo in resInfoList) {
                val packageName = resolveInfo.activityInfo.packageName
                grantUriPermission(
                    packageName,
                    captureVideoFile,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
                )
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (takeVideoIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE)
        }
    }

    /*============================    Shared Preferences   =====================================*/

    fun putStringDataInPreferences(key: String, value: String?) {
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString(key, value).apply()
    }

    fun getStringDataFromPreferences(key: String): String? {
        return PreferenceManager.getDefaultSharedPreferences(this).getString(key, "")
    }

    fun putBooleanDataInPreferences(key: String, value: Boolean?) {
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean(key, value!!).apply()
    }

    fun getBooleanDataFromPreferences(key: String): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(key, false)
    }

    fun clearPreferences() {
        try {
            val lastLoggedInUserId = getStringDataFromPreferences(Constants.LAST_LOGGED_IN_USER)
            val fcmToken = getStringDataFromPreferences(Constants.PREFERENCE_FCM_TOKEN)
            PreferenceManager.getDefaultSharedPreferences(this).edit().clear().apply()
            putStringDataInPreferences(Constants.LAST_LOGGED_IN_USER, lastLoggedInUserId)
            putStringDataInPreferences(Constants.PREFERENCE_FCM_TOKEN, fcmToken)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun saveAndRedirectResponse(response: Resource<JsonObjectResponse<UserModel>>?) {
        try{
            putStringDataInPreferences(
                PREFERENCE_KEY_USER_ID,
                response?.data?.`object`?.id)

            startActivity(
                Intent(
                    this,
                    DashboardActivity::class.java
                ).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun editAndSaveUser(response: Resource<JsonObjectResponse<UserModel>>) {

      /*  putStringDataInPreferences(
            Constants.PREFERENCE_KEY_PROFILE_PIC,
            response.data?.`object`?.profilePic
        )
        putStringDataInPreferences(
            Constants.PREFERENCE_KEY_USER_ID,
            response.data?.`object`?.id
        )*/
        val gson = Gson()
        putStringDataInPreferences(
            Constants.PREFERENCE_KEY_USER_DATA,
            gson.toJson(response.data?.`object`)
        )
        finish()
    }


    fun handleError(code: String?) {
        if (code != null && Constants.ERROR_440 == code) {
            //Logout
        }
    }

    fun processHeaders(headers: Headers?) {
        try {
            val updateAvailable = headers!!.get("update_available")
            val forceUpdateRequired = headers.get("force_update")
            if ("1".equals(updateAvailable)) {
                if ("1".equals(forceUpdateRequired)) {
                    try {
                        getAlertDialogBuilder(
                            getString(R.string.update_available),
                            getString(R.string.new_version_available),
                            false
                        ).setPositiveButton(
                            getString(R.string.update),
                            DialogInterface.OnClickListener { dialogInterface, i -> redirectToPlayStore() }
                        ).show()
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                } else {
                    if (SupplyBeam.dialogShownForUpdate != 1) {
                        try {
                            getAlertDialogBuilder(
                                getString(R.string.update_available),
                                getString(R.string.new_version_available_optional),
                                true
                            ).setNegativeButton(
                                getString(R.string.no), null
                            ).setPositiveButton(
                                getString(R.string.yes),
                                DialogInterface.OnClickListener { dialogInterface, i -> redirectToPlayStore() }
                            ).show()
                            SupplyBeam.dialogShownForUpdate = 1
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    fun redirectToPlayStore() {
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setData(Uri.parse("market://details?id=" + getApplicationContext().getPackageName()))
            startActivity(intent)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onClick(v: View?) {

    }

    fun isCameraPermission(): Boolean{
        if (Build.VERSION.SDK_INT >= 23){
            if(ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    Constants.PERMISSION_CAMERA,
                    Constants.PERMISSION_REQUEST_QR_CAMERA
                )
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }

    fun hideKeyboard(){
        try{
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(findViewById<View>(android.R.id.content).getWindowToken(), 0);
        } catch(e: Exception){
            e.printStackTrace()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when(requestCode){



            Constants.PERMISSION_REQUEST_QR_CAMERA ->
                if(grantResults.size > 0  && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                } else{

                }

            /*if(grantResults.size > 0  && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){

                } else{

                }*/

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {

            PERMISSION_REQUEST_QR_CAMERA -> if(resultCode == Activity.RESULT_OK){

            }

        }
    }

}