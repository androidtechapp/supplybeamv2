package org.genuinemark.supplybeam.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import org.genuinemark.supplybeam.model.ChatMessageData

@Dao
interface ChatMessageDao {
    @Query("SELECT * from chatTable")
    fun getAll(): List<ChatMessageData>

    @Insert(onConflict = REPLACE)
    fun insert(vararg chatMessageData: ChatMessageData)

    @Query("DELETE from chatTable")
    fun deleteAll()

    @Query("SELECT * FROM chatTable WHERE  chatTable.status LIKE 0 GROUP BY chatTable.conversation_id")
    fun getUnreadMessage(): List<ChatMessageData>
}