package org.genuinemark.supplybeam.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import org.genuinemark.supplybeam.model.ChatMessageData

@Database(entities = arrayOf(ChatMessageData::class), version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    companion object {
        private var INSTANCE: AppDatabase? = null
        fun initDB(context: Context) {
            synchronized(AppDatabase::class) {
                INSTANCE = Room.databaseBuilder(
                    context.getApplicationContext(),
                    AppDatabase::class.java, "yogous.db"
                ).allowMainThreadQueries().build()
            }
        }

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                initDB(context)
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

    abstract fun chatMessageDao(): ChatMessageDao
}