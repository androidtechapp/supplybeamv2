package org.genuinemark.supplybeam.binding

import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.text.Html
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextPaint
import android.text.style.AbsoluteSizeSpan
import android.text.style.MetricAffectingSpan
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import org.genuinemark.supplybeam.utils.Constants
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt


object DataBindingAdapter {

    @BindingAdapter("src_url", "placeholder")
    @JvmStatic
    fun loadImage(imageView: ImageView, sourceUrl: String?, drawable: Drawable?) {
        if (sourceUrl == null) {
            imageView.setImageDrawable(drawable)
        } else {
            Glide.with(imageView.context)
                .setDefaultRequestOptions(RequestOptions().placeholder(drawable).error(drawable))
                .load(sourceUrl)
                .error(drawable)
                .into(imageView)
        }
    }

    @BindingAdapter("src_url", "placeholder", "cornerI")
    @JvmStatic
    fun loadImageWithRoundedCorner(
        imageView: ImageView,
        sourceUrl: String?,
        drawable: Drawable?,
        corner: Int
    ) {
        if (sourceUrl == null) {
            imageView.setImageDrawable(drawable)
        } else {
            Glide.with(imageView.context)
                .setDefaultRequestOptions(RequestOptions().placeholder(drawable).error(drawable))
                .load(sourceUrl)
                .transform(CenterCrop())
                .transform(RoundedCorners(corner))
                .error(drawable)
                .into(imageView)
        }
    }

    @BindingAdapter("src_url", "placeholder", "corner")
    @JvmStatic
    fun loadThumbFromVideoWithRoundedCorner(
        imageView: ImageView,
        videoUri: Uri?,
        drawable: Drawable?,
        corner: Int
    ) {
        if (videoUri == null) {
            imageView.setImageDrawable(drawable)
        } else {
            Glide.with(imageView.context)
                .setDefaultRequestOptions(RequestOptions().placeholder(drawable).error(drawable))
                .load(videoUri)
                .thumbnail(Glide.with(imageView.context).load(videoUri))
                .transform(CenterCrop())
                .transform(RoundedCorners(corner))
                .error(drawable)
                .into(imageView)
        }
    }

    @BindingAdapter("time_ago")
    @JvmStatic
    fun getTimeAgo(textView: AppCompatTextView, dateString: String?) {
        if (dateString == null) {
            return
        }

        val SECOND_MILLIS = 1000
        val MINUTE_MILLIS = 60 * SECOND_MILLIS
        val HOUR_MILLIS = 60 * MINUTE_MILLIS
        val DAY_MILLIS = 24 * HOUR_MILLIS

        val date: Date

        if (dateString.contains("T")) {
            val simpleDateFormat =
                SimpleDateFormat(Constants.DATE_FORMAT_YYYY_MM_DD_HH_mm_ss_SSS, Locale.US)
            simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")

            date = simpleDateFormat.parse(dateString)!!
        } else {
            val c = Calendar.getInstance()
            c.timeInMillis = dateString.toLongOrNull()?.times(1000L) ?: 0
            date = c.time
        }

        var time = date.time
        if (time < 1000000000000L) {
            time *= 1000
        }

        val now = currentDate().time
        if (time > now || time <= 0) {
            //textView.text = "in the future"
        }

        val diff = now - time
        when {
            //diff < MINUTE_MILLIS -> textView.text = "moments ago"
            //diff < 2 * MINUTE_MILLIS -> textView.text = "a minute ago"
            diff < 2000L -> textView.text = "${diff / SECOND_MILLIS} sec ago"
            diff < MINUTE_MILLIS -> textView.text = "${diff / SECOND_MILLIS} secs ago"
            diff < 2 * MINUTE_MILLIS -> textView.text = "${diff / MINUTE_MILLIS} min ago"
            diff < 60 * MINUTE_MILLIS -> textView.text = "${diff / MINUTE_MILLIS} mins ago"
            diff < 2 * HOUR_MILLIS -> textView.text = "1 hour ago"
            diff < 24 * HOUR_MILLIS -> textView.text = "${diff / HOUR_MILLIS} hours ago"
            //diff < 48 * HOUR_MILLIS -> "yesterday"
            diff < 24 * HOUR_MILLIS -> textView.text = "1 day ago"
            else -> textView.text = "${diff / DAY_MILLIS} days ago"
        }
    }

    fun getTimeAgoFollow(dateString: String?): String {
        if (dateString == null) {
            return ""
        }
        val SECOND_MILLIS = 1000
        val MINUTE_MILLIS = 60 * SECOND_MILLIS
        val HOUR_MILLIS = 60 * MINUTE_MILLIS
        val DAY_MILLIS = 24 * HOUR_MILLIS

        val date: Date

        if (dateString.contains("T")) {
            val simpleDateFormat =
                SimpleDateFormat(Constants.DATE_FORMAT_YYYY_MM_DD_HH_mm_ss_SSS, Locale.US)
            simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")

            date = simpleDateFormat.parse(dateString)!!
        } else {
            val c = Calendar.getInstance()
            c.timeInMillis = dateString.toLongOrNull()?.times(1000L) ?: 0
            date = c.time
        }

        var time = date.time
        if (time < 1000000000000L) {
            time *= 1000
        }

        val now = currentDate().time
        if (time > now || time <= 0) {
            //textView.text = "in the future"
        }

        val diff = now - time
        when {
            //diff < MINUTE_MILLIS -> textView.text = "moments ago"
            //diff < 2 * MINUTE_MILLIS -> textView.text = "a minute ago"
            diff < MINUTE_MILLIS -> return "${diff / SECOND_MILLIS}s"
            diff < 60 * MINUTE_MILLIS -> return "${diff / MINUTE_MILLIS}m"
            //diff < 2 * HOUR_MILLIS -> return "an hour ago"
            diff < 24 * HOUR_MILLIS -> return "${diff / HOUR_MILLIS}h"
            //diff < 48 * HOUR_MILLIS -> "yesterday"
            else -> return "${diff / DAY_MILLIS}d"
        }
    }

    private fun currentDate(): Date {
        val calendar = Calendar.getInstance()
        return calendar.time
    }

/*    @BindingAdapter("load_image_progress")
    @JvmStatic
    fun loadImageWithProgress(imageView: ImageView, sourceUrl: String?) {
        val circularProgressDrawable = CircularProgressDrawable(imageView.context)
        circularProgressDrawable.strokeWidth = 7f
        circularProgressDrawable.centerRadius = 30f
        //circularProgressDrawable.setColorSchemeColors(ContextCompat.getColor(view.getContext(), R.color.green));
        circularProgressDrawable.start()

        if (sourceUrl == null) {
            imageView.setImageResource(R.drawable.feed_image_placeholder)
        } else {
            Glide.with(imageView.context)
                .load(sourceUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(circularProgressDrawable)
                .error(R.drawable.feed_image_placeholder)
                .into(imageView)
        }
    }*/


    @BindingAdapter("roundOffAmount")
    @JvmStatic
    fun roundOffAmountToInt(textView: AppCompatTextView, value: Double?) {
        if (value == null) {
            return
        }

        textView.text = "$" + value.roundToInt()
    }

    @BindingAdapter("dateFromTimeStamp")
    @JvmStatic
    fun dateFromTimeStamp(textView: AppCompatTextView, time: Long?) {
        if (time == null) {
            return
        }
        val simpleDateFormat =
            SimpleDateFormat(Constants.DATE_FORMAT_MM_DD_YYYY, Locale.US)

        val date = simpleDateFormat.format(Date(time * 1000))

        textView.text = date
    }


    @BindingAdapter("formatCurrency")
    @JvmStatic
    fun formatCurrency(textView: AppCompatTextView, amount: Double) {
        val decimalFormat = DecimalFormat("$0.00")
        textView.text = decimalFormat.format(amount)
    }


}