package org.genuinemark.supplybeam.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import org.genuinemark.supplybeam.SupplyBeam
import org.genuinemark.supplybeam.api.input.*
import org.genuinemark.supplybeam.api.response.JsonObjectResponse
import org.genuinemark.supplybeam.api.response.Resource
import org.genuinemark.supplybeam.models.CheckInModel
import org.genuinemark.supplybeam.models.CheckInOutModel
import org.genuinemark.supplybeam.models.DashboardModel
import org.genuinemark.supplybeam.models.SendOtpModel
import org.genuinemark.supplybeam.repositories.DashboardRepository
import org.genuinemark.supplybeam.repositories.LoginRepository
import org.genuinemark.supplybeam.utils.AbsentLiveData

class HomeViewModel (
    application: Application
): AndroidViewModel(application){


    private var dashboardInput = MutableLiveData<DashboardInput>()
    private var checkInInput = MutableLiveData<CheckInInput>()
    private var checkINkOutInput = MutableLiveData<CheckInOutInput>()
    private var dashboardRepository: DashboardRepository = DashboardRepository((application as SupplyBeam).provideApiService())


    fun setOtpInput(otpInput: DashboardInput){
        this.dashboardInput.value = otpInput
    }

    fun setCheckInInput(checkInInput: CheckInInput){
        this.checkInInput.value = checkInInput
    }


    fun setCheckInOutInput(checkInOutInput: CheckInOutInput){
        this.checkINkOutInput.value = checkInOutInput
    }

    private val getUserInfoObservable: LiveData<Resource<JsonObjectResponse<DashboardModel>>>? =
        Transformations
            .switchMap(dashboardInput) { dashboardInput ->
                if (dashboardInput == null) {
                    AbsentLiveData.create()
                } else {
                    dashboardRepository.userInfo(dashboardInput)
                }
            }

    fun getUserInfoObservable(): LiveData<Resource<JsonObjectResponse<DashboardModel>>>? {
        return getUserInfoObservable
    }

    /**
     * Call Repo for CheckIn API
     */
    private val getCheckInObservable: LiveData<Resource<JsonObjectResponse<CheckInModel>>>? =
        Transformations
            .switchMap(checkInInput) { checkInInput ->
                if (checkInInput == null) {
                    AbsentLiveData.create()
                } else {
                    dashboardRepository.checkInInfo(checkInInput)
                }
            }

    fun getCheckInObservable(): LiveData<Resource<JsonObjectResponse<CheckInModel>>>? {
        return getCheckInObservable
    }


    /**
     * Call Repo for CheckInOUT API
     */
    private val getCheckInOutObservable: LiveData<Resource<JsonObjectResponse<CheckInOutModel>>>? =
        Transformations
            .switchMap(checkINkOutInput) { checkINkOutInput ->
                if (checkINkOutInput == null) {
                    AbsentLiveData.create()
                } else {
                    dashboardRepository.checkInOutInfo(checkINkOutInput)
                }
            }

    fun getCheckInOUtObservable(): LiveData<Resource<JsonObjectResponse<CheckInOutModel>>>? {
        return getCheckInOutObservable
    }

}