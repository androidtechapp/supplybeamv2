package org.genuinemark.supplybeam.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import org.genuinemark.supplybeam.api.input.LoginInput
import org.genuinemark.supplybeam.api.response.JsonObjectResponse
import org.genuinemark.supplybeam.api.response.Resource
import org.genuinemark.supplybeam.model.UserModel
import org.genuinemark.supplybeam.repositories.LoginRepository
import org.genuinemark.supplybeam.utils.AbsentLiveData
import javax.inject.Inject


class LoginViewModel (
    application: Application
) : AndroidViewModel(application) {

    private var loginInput = MutableLiveData<LoginInput>()


    fun setLoginInput(loginInput: LoginInput) {
        this.loginInput.value = loginInput
    }


    /*private val getUserLoginObservable: LiveData<Resource<JsonObjectResponse<UserModel>>>? =
        Transformations
            .switchMap(loginInput) { loginInput ->
                if (loginInput == null) {
                    AbsentLiveData.create()
                } else {
                    //loginRepository.loginUser(loginInput)
                }
            }

    fun getUserLoginObservable(): LiveData<Resource<JsonObjectResponse<UserModel>>>? {
        return getUserLoginObservable
    }*/


}