package org.genuinemark.supplybeam.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import org.genuinemark.supplybeam.SupplyBeam
import org.genuinemark.supplybeam.api.input.LoginInput
import org.genuinemark.supplybeam.api.input.OTPInput
import org.genuinemark.supplybeam.api.response.JsonObjectResponse
import org.genuinemark.supplybeam.api.response.Resource
import org.genuinemark.supplybeam.model.UserModel
import org.genuinemark.supplybeam.models.SendOtpModel
import org.genuinemark.supplybeam.repositories.LoginRepository
import org.genuinemark.supplybeam.utils.AbsentLiveData

class OtpVerifyViewModel (
    application: Application
    ) : AndroidViewModel(application) {

    private var otpInput = MutableLiveData<OTPInput>()
    private var loginInput = MutableLiveData<LoginInput>()
    private var loginRepository: LoginRepository = LoginRepository((application as SupplyBeam).provideApiService())


    fun setOtpVerify(loginInput: LoginInput){
        this.loginInput.value = loginInput
    }

    fun setOtpInput(otpInput: OTPInput){
        this.otpInput.value = otpInput
    }

    private val getUserLoginObservable: LiveData<Resource<JsonObjectResponse<SendOtpModel>>>? =
        Transformations
            .switchMap(otpInput) { otpInput ->
                if (otpInput == null) {
                    AbsentLiveData.create()
                } else {
                    loginRepository.loginUser(otpInput)
                }
            }

    fun getUserLoginObservable(): LiveData<Resource<JsonObjectResponse<SendOtpModel>>>? {
        return getUserLoginObservable
    }


    private val getOtpVerifyObservable: LiveData<Resource<JsonObjectResponse<UserModel>>>? =
        Transformations
            .switchMap(loginInput) { loginInput ->
                if (loginInput == null) {
                    AbsentLiveData.create()
                } else {
                    loginRepository.otpVerify(loginInput)
                }
            }

    fun getOtpVerifyObservable(): LiveData<Resource<JsonObjectResponse<UserModel>>>? {
        return getOtpVerifyObservable
    }


}