package org.genuinemark.supplybeam.api.response


enum class Status {
    SUCCESS,
    ERROR,
    FAILURE,
    LOADING,
    FORCE_UPDATE
}