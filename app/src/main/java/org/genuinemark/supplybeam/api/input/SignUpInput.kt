package org.genuinemark.supplybeam.api.input

import org.genuinemark.supplybeam.base.BaseModel
import com.google.gson.annotations.SerializedName

data class SignUpInput(

    @field:SerializedName("business_name")
    var businessName: String? = null,

    @field:SerializedName("role")
    var role: Int? = null,

    @field:SerializedName("address")
    var address: String? = null,

    @field:SerializedName("isd_code")
    var isdCode: String? = null,

    @field:SerializedName("gender")
    var gender: String? = null,

    @field:SerializedName("lng")
    var lng: String? = null,

    @field:SerializedName("city")
    var city: String? = null,

    @field:SerializedName("mobile")
    var mobile: String? = null,

    @field:SerializedName("last_name")
    var lastName: String? = null,

    @field:SerializedName("middle_name")
    var middleAme: String? = null,

    @field:SerializedName("zipcode")
    var zipcode: String? = null,

    @field:SerializedName("password")
    var password: String? = null,

    @field:SerializedName("social_id")
    var socialId: String? = null,

    @field:SerializedName("social_type")
    var socialType: String? = null,

    @field:SerializedName("home_cso")
    var homeCso: List<String?>? = null,

    @field:SerializedName("dob")
    var dob: String? = null,

    @field:SerializedName("device_token")
    var deviceToken: String? = null,

    @field:SerializedName("state")
    var state: String? = null,

    @field:SerializedName("first_name")
    var firstName: String? = null,

    @field:SerializedName("email")
    var email: String? = null,

    @field:SerializedName("lat")
    var lat: String? = null,

    @field:SerializedName("username")
    var username: String? = null,

    @field:SerializedName("social_image_url")
    var socialImageUrl: String? = null
) : BaseModel()