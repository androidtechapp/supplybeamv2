package org.genuinemark.supplybeam.api.input

import com.google.gson.annotations.SerializedName

data class CheckInInput (

    @field:SerializedName("vendorId")
    val vendorId: String? = null
)