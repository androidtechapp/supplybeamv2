package org.genuinemark.supplybeam.api.response

import org.genuinemark.supplybeam.base.BaseResponse
import com.google.gson.annotations.SerializedName



/*This class to get all Object type response*/
class JsonObjectResponse<T> : BaseResponse(){
    @SerializedName("data")
    var `object`: T? = null
}