package org.genuinemark.supplybeam.api.input

import org.genuinemark.supplybeam.base.BaseModel
import com.google.gson.annotations.SerializedName

data class OTPInput(
    @field:SerializedName("mobileNo")
    val mobileNo: String? = null
)
