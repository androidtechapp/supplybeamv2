package org.genuinemark.supplybeam.api.response

import okhttp3.Headers


// A generic class that contains data and status about loading this data.
data class Resource<out T>(val status: Status, val data: T?, val message: String?,var headers: Headers? = null) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(Status.FAILURE, data, msg)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }
    }
}
