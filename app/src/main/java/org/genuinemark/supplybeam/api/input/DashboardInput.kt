package org.genuinemark.supplybeam.api.input

import com.google.gson.annotations.SerializedName

data class DashboardInput (
    @field:SerializedName("userId")
    val userId: String? = null
)