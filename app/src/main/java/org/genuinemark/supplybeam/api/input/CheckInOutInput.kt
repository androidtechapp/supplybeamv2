package org.genuinemark.supplybeam.api.input

import com.google.gson.annotations.SerializedName

data class CheckInOutInput (

    @field:SerializedName("batchId")
    val vendorId: String? = null
)