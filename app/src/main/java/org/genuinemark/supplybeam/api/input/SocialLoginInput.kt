package org.genuinemark.supplybeam.api.input

import com.google.gson.annotations.SerializedName

data class SocialLoginInput(

    @field:SerializedName("social_id")
    val socialId: String? = null,

    @field:SerializedName("social_type")
    val socialType: String? = null,

    @field:SerializedName("device_tokens")
    val deviceTokens: String? = null
)