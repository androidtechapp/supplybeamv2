package org.genuinemark.supplybeam.api.response

import org.genuinemark.supplybeam.base.BaseResponse
import com.google.gson.annotations.SerializedName



/*This class to get all List type response*/
class JsonArrayResponse<T> : BaseResponse(){
    @SerializedName("data")
    var list: List<T>? = null
}