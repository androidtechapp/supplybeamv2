package org.genuinemark.supplybeam.api.response


import androidx.databinding.library.BuildConfig
import org.genuinemark.supplybeam.api.input.*
import org.genuinemark.supplybeam.model.UserModel
import org.genuinemark.supplybeam.base.BaseResponse
import org.genuinemark.supplybeam.models.CheckInModel
import org.genuinemark.supplybeam.models.CheckInOutModel
import org.genuinemark.supplybeam.models.DashboardModel
import org.genuinemark.supplybeam.models.SendOtpModel
import org.genuinemark.supplybeam.utils.Constants
import retrofit2.Call
import retrofit2.http.*



interface ApiService {

    @POST("sendOtp")
    @Headers(
        "Content-Type: application/json",
        "Accept-Version: " + BuildConfig.VERSION_NAME,
        "Device-Os: " + Constants.OS,
        "Accept-Language: en"
    )
    fun loginUser(@Body otpInput: OTPInput): Call<JsonObjectResponse<SendOtpModel>>

    @POST("dashboard")
    @Headers(
        "Content-Type: application/json",
        "Accept-Version: " + BuildConfig.VERSION_NAME,
        "Device-Os: " + Constants.OS,
        "Accept-Language: en"
    )
    fun userInfo(@Body dashboardInput: DashboardInput): Call<JsonObjectResponse<DashboardModel>>


    @POST("checkIn")
    @Headers(
        "Content-Type: application/json",
        "Accept-Version: " + BuildConfig.VERSION_NAME,
        "Device-Os: " + Constants.OS,
        "Accept-Language: en"
    )
    fun checkInInfo(@Body checkInInput: CheckInInput): Call<JsonObjectResponse<CheckInModel>>


    @POST("checkInOut")
    @Headers(
        "Content-Type: application/json",
        "Accept-Version: " + BuildConfig.VERSION_NAME,
        "Device-Os: " + Constants.OS,
        "Accept-Language: en"
    )
    fun checkInInfo(@Body checkInOutInput: CheckInOutInput): Call<JsonObjectResponse<CheckInOutModel>>

    @POST("login")
    @Headers(
        "Content-Type: application/json",
        "Accept-Version: " + BuildConfig.VERSION_NAME,
        "Device-Os: " + Constants.OS,
        "Accept-Language: en"
    )
    fun otpVerifyUser(@Body loginInput: LoginInput): Call<JsonObjectResponse<UserModel>>



    @POST("/auth/is_username_available")
    @Headers(
        "Content-Type: application/json",
        "Accept-Version: " + BuildConfig.VERSION_NAME,
        "Device-Os: " + Constants.OS,
        "Accept-Language: en"
    )
    fun getIsUsernameAvailable(@Body hashMap: HashMap<String, String>): Call<JsonObjectResponse<BaseResponse>>


    @POST("/auth/register")
    @Headers(
        "Content-Type: application/json",
        "Accept-Version: " + BuildConfig.VERSION_NAME,
        "Device-Os: " + Constants.OS,
        "Accept-Language: en"
    )
    fun registerUser(@Body signUpInput: SignUpInput): Call<JsonObjectResponse<UserModel>>




}