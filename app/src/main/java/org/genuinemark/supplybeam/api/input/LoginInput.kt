package org.genuinemark.supplybeam.api.input

import org.genuinemark.supplybeam.base.BaseModel
import com.google.gson.annotations.SerializedName


data class LoginInput(

    @field:SerializedName("otp")
    val otp: String? = null,

    @field:SerializedName("mobileNo")
    val mobileNo: String? = null
)