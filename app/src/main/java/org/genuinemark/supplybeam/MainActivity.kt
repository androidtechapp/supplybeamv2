package org.genuinemark.supplybeam

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import org.genuinemark.supplybeam.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
